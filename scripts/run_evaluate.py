#!/usr/bin/env python
# coding: utf-8

# In[1]:


from nuscenes import NuScenes
from nuscenes.eval.common.config import config_factory
from nuscenes.eval.common.data_classes import EvalBoxes
from nuscenes.eval.common.loaders import load_prediction, load_gt, add_center_dist, filter_eval_boxes
from nuscenes.eval.detection.algo import accumulate, calc_ap, calc_tp
from nuscenes.eval.detection.constants import TP_METRICS
from nuscenes.eval.detection.data_classes import DetectionConfig, DetectionMetrics, DetectionBox,     DetectionMetricDataList
from nuscenes.eval.detection.render import summary_plot, class_pr_curve, class_tp_curve, dist_pr_curve, visualize_sample


# In[4]:


import evaluate
import os
if __name__ == "__main__":
    print('evaluate start')
# In[ ]:
    #Baseline
    result_path_s =['/Users/mike/Desktop/data/sets/out_put_dir/baseline/lidar_mini_val.json',  
                    '/Users/mike/Desktop/data/sets/out_put_dir/baseline/camera_mini_val.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/baseline/centerpoint_voxel_circlenms_mini_val.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/baseline/pointpillars_secfpn_mini_val.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/baseline/fcos_mini_val.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/baseline/GT.json',
                    ]
    '''
    #nms combination union 
    
    result_path_s =['/Users/mike/Desktop/data/sets/out_put_dir/NMS/Union/D1_D2.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Union/D1_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Union/D1_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Union/D2_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Union/D2_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Union/D3_D4.json',
                    ]
    
    #cd combination union
    result_path_s =[
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Union/D1_D2.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Union/D1_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Union/D1_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Union/D2_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Union/D2_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Union/D3_D4.json',]
    
    #WBF combination
    
    result_path_s = ['/Users/mike/Desktop/data/sets/out_put_dir/WBF/Union/D1_D2.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Union/D1_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Union/D1_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Union/D2_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Union/D2_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Union/D3_D4.json',]
               
    #nms combination intersection 
    
    result_path_s = ['/Users/mike/Desktop/data/sets/out_put_dir/NMS/Inter/D1_D2.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Inter/D1_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Inter/D1_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Inter/D2_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Inter/D2_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/NMS/Inter/D3_D4.json',
                    ]
    
    #CD combination intersection 
    
    result_path_s = ['/Users/mike/Desktop/data/sets/out_put_dir/CD/Inter/D1_D2.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Inter/D1_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Inter/D1_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Inter/D2_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Inter/D2_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/CD/Inter/D3_D4.json',
                    ]
    
    #Addition
    result_path_s = ['/Users/mike/Desktop/data/sets/out_put_dir/Add/D1_D2.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/Add/D1_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/Add/D1_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/Add/D2_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/Add/D2_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/Add/D3_D4.json',
                    ]
    #WBF combination intersection
    result_path_s = ['/Users/mike/Desktop/data/sets/out_put_dir/WBF/Inter/D1_D2.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Inter/D1_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Inter/D1_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Inter/D2_D3.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Inter/D2_D4.json',
                    '/Users/mike/Desktop/data/sets/out_put_dir/WBF/Inter/D3_D4.json',]
    '''
    #pre NMS
    result_path_s = ['/Users/mike/Desktop/MasterThesis/dataset/center_distance/same_sensor/cd_best/mapillary&fcos.json',
                 '/Users/mike/Desktop/MasterThesis/dataset/center_distance/same_sensor/cd_best/pointpillars_secfpn&centerpoint.json',
                     '/Users/mike/Desktop/MasterThesis/dataset/center_distance/same_sensor/cd_best/pointpillars&centerpoint.json',
                     '/Users/mike/Desktop/MasterThesis/dataset/center_distance/same_sensor/cd_best/pointpillars_secfpn&pointpillars.json',
                        
                    ]

    #baseline
    index = ['pointpillar_lidar','mapillary_camera','centerpoint_lidar','pointpillar_secfpn_lidar','fcos','GT']

    #combination



    index = ['mapillary&fcos','pointpillars_secfpn&centerpoint','pointpillars&centerpoint','pointpillars_secfpn&pointpillars']

    evaluate.NuScenesEval.start(result_path_s,index,'cd_same')



    #0.4513
    #0.8 0.6 0.6



