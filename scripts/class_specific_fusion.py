#!/usr/bin/env python
# coding: utf-8

# In[3]:


import json
import torch
import math
import numpy as np
from collections import OrderedDict
from pytorch3d.ops.iou_box3d import box3d_overlap
from ensemble_boxes import *
import time
import os
from typing import Tuple

import torch
import torch.nn.functional as F
from pytorch3d import _C
from torch.autograd import Function


# In[4]:


#save as json file
#meta = {'use_camera': True, 'use_lidar': False,'use_radar': False, 'use_map': False, 'use_external': False}
#results = data
def create_and_save_json(data,name):
    meta = {'use_camera': True, 'use_lidar': False,
                     'use_radar': False, 'use_map': False, 'use_external': False}
    json_file = {"meta":meta}
    json_file['results'] = data
    cwd = os.getcwd()
    path = cwd + '/' + name +".json"
    out_file = open(path, "w")
    print('save to :',path)
    json.dump(json_file, out_file)
    
    out_file.close()


# In[5]:


#provide the eight corners in space XYZ
#input box_info
#output shape (8,3)
def box_center_to_corner(translation,size,rotation):
    #translation = box_info['translation']
    #size = box_info['size']
    #rotation = box_info['rotation']
    translation = translation
    size = size
    rotation = rotation
    # To return
    corner_boxes = np.zeros((8, 3))

    translation = translation
    h, w, l = size[0], size[1], size[2]
    W,X,Y,Z = rotation

    # Create a bounding box outline
    bounding_box = np.array([
        [-l/2, -l/2, l/2, l/2, -l/2, -l/2, l/2, l/2],
        [w/2, -w/2, -w/2, w/2, w/2, -w/2, -w/2, w/2],
        [-h/2, -h/2, -h/2, -h/2, h/2, h/2, h/2, h/2]])

    # Standard 3x3 rotation matrix around the Z axis
    rotation_matrix = quatRotation(W,X,Y,Z)
    #rotation_matrix = rotation_matrix[:3]
    #print(rotation_matrix)
    # Repeat the [x, y, z] eight times
    eight_points = np.tile(translation, (8, 1))

    # Translate the rotated bounding box by the
    # original center position to obtain the final box
    corner_box = np.dot(
        rotation_matrix, bounding_box) + eight_points.transpose()

    return corner_box.transpose()
#Return a transfer matrics
#input Quaternion
#output 3X3 matrix
def quatRotation(W,X,Y,Z):
    """
    Covert a quaternion into a full three-dimensional rotation matrix.
 
    Input
    :param Q: A 4 element array representing the quaternion (q0,q1,q2,q3) 
 
    Output
    :return: A 3x3 element matrix representing the full 3D rotation matrix. 
             This rotation matrix converts a point in the local reference 
             frame to a point in the global reference frame.
    """
    # Extract the values from Q
    q0 = math.cos(W/2)
    q1 = X*math.sin(W/2)
    q2 = Y*math.sin(W/2)
    q3 = Z*math.sin(W/2)
     
    # First row of the rotation matrix
    r00 = 2 * (q0 * q0 + q1 * q1) - 1
    r01 = 2 * (q1 * q2 - q0 * q3)
    r02 = 2 * (q1 * q3 + q0 * q2)
     
    # Second row of the rotation matrix
    r10 = 2 * (q1 * q2 + q0 * q3)
    r11 = 2 * (q0 * q0 + q2 * q2) - 1
    r12 = 2 * (q2 * q3 - q0 * q1)
     
    # Third row of the rotation matrix
    r20 = 2 * (q1 * q3 - q0 * q2)
    r21 = 2 * (q2 * q3 + q0 * q1)
    r22 = 2 * (q0 * q0 + q3 * q3) - 1
     
    # 3x3 rotation matrix
    mat = np.array([[r00, r01, r02],
                           [r10, r11, r12],
                           [r20, r21, r22]])
                            
    return mat


# In[6]:

'''
def load_mini_val(detector1,detector2):
    path_1 = '/Users/mike/Desktop/data/sets/out_put_dir/baseline/'+ detector1 +'.json'
    path_2 = '/Users/mike/Desktop/data/sets/out_put_dir/baseline/'+ detector2 +'.json'
    if not os.path.isfile(path_1):
        print(path_1)
        raise ValueError('Detector 1 not found')
        
    if not os.path.isfile(path_2):
        raise ValueError('Detector 2 not found')
    with open(path_1) as f:
        data_camera = json.load(f)
    #print(type(data_camera))
    with open(path_2) as d:
        data_lidar = json.load(d)
    #print(data_camera['meta'])
    print(data_lidar['meta'])
    result_camera = data_camera['results']
    result_lidar = data_lidar['results']
    frame_camera = []
    frame_lidar = []
    frames = []
    #print (result_camera.keys())
    for i in result_camera.keys():
        frame_camera.append(i)
    for j in result_lidar.keys():
        frame_lidar.append(j)
    print (type(frame_lidar))
    frames = frame_camera
    print ('frame camera',len(frame_camera))
    print ('frame lidar',len(frame_lidar))
    return(result_camera,result_lidar,frames)
'''

# In[35]:



# In[7]:


# Copyright (c) Meta Platforms, Inc. and affiliates.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree.
# 



# -------------------------------------------------- #
#                  CONSTANTS                         #
# -------------------------------------------------- #
"""
_box_planes and _box_triangles define the 4- and 3-connectivity
of the 8 box corners.
_box_planes gives the quad faces of the 3D box
_box_triangles gives the triangle faces of the 3D box
"""
_box_planes = [
    [0, 1, 2, 3],
    [3, 2, 6, 7],
    [0, 1, 5, 4],
    [0, 3, 7, 4],
    [1, 2, 6, 5],
    [4, 5, 6, 7],
]
_box_triangles = [
    [0, 1, 2],
    [0, 3, 2],
    [4, 5, 6],
    [4, 6, 7],
    [1, 5, 6],
    [1, 6, 2],
    [0, 4, 7],
    [0, 7, 3],
    [3, 2, 6],
    [3, 6, 7],
    [0, 1, 5],
    [0, 4, 5],
]


def _check_coplanar(boxes: torch.Tensor, eps: float = 1e-4) -> None:
    faces = torch.tensor(_box_planes, dtype=torch.int64, device=boxes.device)
    verts = boxes.index_select(index=faces.view(-1), dim=1)
    B = boxes.shape[0]
    P, V = faces.shape
    # (B, P, 4, 3) -> (B, P, 3)
    v0, v1, v2, v3 = verts.reshape(B, P, V, 3).unbind(2)

    # Compute the normal
    e0 = F.normalize(v1 - v0, dim=-1)
    e1 = F.normalize(v2 - v0, dim=-1)
    normal = F.normalize(torch.cross(e0, e1, dim=-1), dim=-1)

    # Check the fourth vertex is also on the same plane
    mat1 = (v3 - v0).view(B, 1, -1)  # (B, 1, P*3)
    mat2 = normal.view(B, -1, 1)  # (B, P*3, 1)
    if not (mat1.bmm(mat2).abs() < eps).all().item():
        msg = "Plane vertices are not coplanar!!!"
        return 0
        #raise ValueError(msg)

    return


def _check_nonzero(boxes: torch.Tensor, eps: float = 1e-4) -> None:
    """
    Checks that the sides of the box have a non zero area
    """
    faces = torch.tensor(_box_triangles, dtype=torch.int64, device=boxes.device)
    verts = boxes.index_select(index=faces.view(-1), dim=1)
    B = boxes.shape[0]
    T, V = faces.shape
    # (B, T, 3, 3) -> (B, T, 3)
    v0, v1, v2 = verts.reshape(B, T, V, 3).unbind(2)

    normals = torch.cross(v1 - v0, v2 - v0, dim=-1)  # (B, T, 3)
    face_areas = normals.norm(dim=-1) / 2

    if (face_areas < eps).any().item():
        msg = "Planes have zero areas"
        raise ValueError(msg)

    return


class _box3d_overlap(Function):
    """
    Torch autograd Function wrapper for box3d_overlap C++/CUDA implementations.
    Backward is not supported.
    """

    @staticmethod
    def forward(ctx, boxes1, boxes2):
        """
        Arguments defintions the same as in the box3d_overlap function
        """
        vol, iou = _C.iou_box3d(boxes1, boxes2)
        return vol, iou

    @staticmethod
    def backward(ctx, grad_vol, grad_iou):
        raise ValueError("box3d_overlap backward is not supported")


def box3d_overlap(
    boxes1: torch.Tensor, boxes2: torch.Tensor, eps: float = 1e-4
) -> Tuple[torch.Tensor, torch.Tensor]:
    """
    Computes the intersection of 3D boxes1 and boxes2.
    Inputs boxes1, boxes2 are tensors of shape (B, 8, 3)
    (where B doesn't have to be the same for boxes1 and boxes2),
    containing the 8 corners of the boxes, as follows:
        (4) +---------+. (5)
            | ` .     |  ` .
            | (0) +---+-----+ (1)
            |     |   |     |
        (7) +-----+---+. (6)|
            ` .   |     ` . |
            (3) ` +---------+ (2)
    NOTE: Throughout this implementation, we assume that boxes
    are defined by their 8 corners exactly in the order specified in the
    diagram above for the function to give correct results. In addition
    the vertices on each plane must be coplanar.
    As an alternative to the diagram, this is a unit bounding
    box which has the correct vertex ordering:
    box_corner_vertices = [
        [0, 0, 0],
        [1, 0, 0],
        [1, 1, 0],
        [0, 1, 0],
        [0, 0, 1],
        [1, 0, 1],
        [1, 1, 1],
        [0, 1, 1],
    ]
    Args:
        boxes1: tensor of shape (N, 8, 3) of the coordinates of the 1st boxes
        boxes2: tensor of shape (M, 8, 3) of the coordinates of the 2nd boxes
    Returns:
        vol: (N, M) tensor of the volume of the intersecting convex shapes
        iou: (N, M) tensor of the intersection over union which is
            defined as: `iou = vol / (vol1 + vol2 - vol)`
    """
    if not all((8, 3) == box.shape[1:] for box in [boxes1, boxes2]):
        raise ValueError("Each box in the batch must be of shape (8, 3)")

    _check_coplanar(boxes1, eps)
    _check_coplanar(boxes2, eps)
    _check_nonzero(boxes1, eps)
    _check_nonzero(boxes2, eps)

    # pyre-fixme[16]: `_box3d_overlap` has no attribute `apply`.
    vol, iou = _box3d_overlap.apply(boxes1, boxes2)

    return vol, iou


# In[8]:


#WBF
def prefilter_boxes_wbf(boxes_info,weights,thr):
    new_boxes = dict()
    for t in range(len(boxes_info)):
        for box_info in boxes_info[t]:
            #print('box_info',box_info)
            
            b = box_info.copy()
            label = box_info['detection_name']
            score = box_info['detection_score']
            if score <= thr:
                continue
            translation = box_info['translation']
            size = box_info['size']
            rotation = box_info['rotation']
            velocity = box_info['velocity']
            sample_token = box_info['sample_token']
            attribute_name = box_info['attribute_name']
            b = [label,score*weights[t],translation,size,rotation,velocity,sample_token,attribute_name]
            if label not in new_boxes:
                new_boxes[label] = []
            new_boxes[label].append(b)
    for k in new_boxes:
        current_boxes = np.array(new_boxes[k],dtype=object)
        new_boxes[k] = current_boxes[current_boxes[:,1].argsort()[::-1]]
        #print('score org',new_boxes[k][:,1])
    #print(new_boxes)
    return new_boxes
def bb_intersection_over_union_3d(A,B):
    t_a,s_a,r_a = A
    t_b,s_b,r_b = B
    Box_A = box_center_to_corner(t_a,s_a,r_a)
    Box_B = box_center_to_corner(t_b,s_b,r_b)

    overlap_vol,iou = box3d_overlap(torch.Tensor(np.array([Box_A])), torch.Tensor(np.array([Box_B])))
        
    return np.array(iou)    

def get_weighted_box(boxes, conf_type='max'):
    #print('get_weighted_box')
    box = np.array(['',0,[],[],[],[],'',''],dtype = object)
    #box = np.zeros(8, dtype=np.float32)
    conf = 0
    conf_list = []
    points = np.zeros((8,3))
    size = np.zeros(3)
    velocity = np.zeros(2)
    for b in boxes:
        
        pts = box_center_to_corner(b[2],b[3],b[4])
        #print(pts.shape)
        #print('b[1]',b[1])
        points = points + (pts*float(b[1]))
        #print('b[3]',b[3])
        size = size + ( np.array(b[3])*float(b[1]))
        velocity = velocity +(np.array(b[5])*float(b[1]))
        conf += b[1]
        conf_list.append(b[1])
    #print('b[1]',conf_list)
    box[0] = boxes[0][0]
    box[4] = boxes[0][4]
    box[6] = boxes[0][6]
    box[7] = boxes[0][7]
    if conf_type == 'avg':
        box[1] = conf / len(boxes)
    elif conf_type == 'max':
        #print('score',conf_list)
        box[1] = np.array(conf_list).max()
        #print('score max',np.array(conf_list).max())
    points /= conf
    size/=conf
    #print('*****',size)
    velocity/=conf
    translation = np.sum(points,axis=0)/8
    box[2] = translation.tolist()
    box[3] = size.tolist()
    ###
    #quaternion
    ###
    box[5] = velocity.tolist()

    return box
    
def find_matching_box(boxes_list, new_box, match_iou):


    best_iou = match_iou
    best_index = -1
    for i in range(len(boxes_list)):
        box = boxes_list[i]
        if box[0] != new_box[0]:
            continue
        iou = bb_intersection_over_union_3d(box[2:5], new_box[2:5])
        if iou > best_iou:
            #print('iou',iou)
            best_index = i
            best_iou = iou


    return best_index, best_iou
def weighted_boxes_fusion_3d(boxes_info, weights, iou_thr, skip_box_thr,intersection, conf_type='max', allows_overflow=False):
    if intersection == True:
        print('Intersection calculating')
    if weights is None:
        weights = np.ones(len(boxes_list))
    if len(weights) != len(boxes_info):
        print('Warning: incorrect number of weights {}. Must be: {}. Set weights equal to 1.'.format(len(weights), len(boxes_list)))
        weights = np.ones(len(boxes_info))
    weights = np.array(weights)
    print(weights,'weights!!!')

    if conf_type not in ['avg', 'max']:
        print('Error. Unknown conf_type: {}. Must be "avg" or "max". Use "avg"'.format(conf_type))
        conf_type = 'avg'

    filtered_boxes = prefilter_boxes_wbf(boxes_info, weights, skip_box_thr)
    if len(filtered_boxes) == 0:
        print('Warning: filtered boxes not found')
        return np.zeros((0, 6)), np.zeros((0,)), np.zeros((0,))

    overall_boxes = []
    for label in filtered_boxes:
        boxes = filtered_boxes[label]
        new_boxes = []
        weighted_boxes = []
        intersection_index = []
        # Clusterize boxes
        for j in range(0, len(boxes)):
            index, best_iou = find_matching_box(weighted_boxes, boxes[j], iou_thr)
            if index != -1:
                new_boxes[index].append(boxes[j])
                weighted_boxes[index] = get_weighted_box(new_boxes[index], conf_type)
                #print('index',index)
            else:
                new_boxes.append([boxes[j].copy()])
                weighted_boxes.append(boxes[j].copy())
            #print('new_boxes, wb',len(new_boxes),len(weighted_boxes),len(intersection_index))
        
        #reset score
        if intersection == True:
            for i in range(len(new_boxes)):
                if len(new_boxes[i])>1:
                    intersection_index.append(i)
                    
            if intersection_index == []:
                break
            weighted_boxes=[weighted_boxes[x] for x in intersection_index]
            new_boxes=[new_boxes[x] for x in intersection_index]
        #print('inter',intersection_index,weighted_boxes)   
        #print('n',len(new_boxes[0]))
            
        for i in range(len(new_boxes)):
            if not allows_overflow:
                weighted_boxes[i][1] = weighted_boxes[i][1] * min(weights.sum(), len(new_boxes[i])) / weights.sum()
            else:
                weighted_boxes[i][1] = weighted_boxes[i][1] * len(new_boxes[i]) / weights.sum()
        overall_boxes.append(np.array(weighted_boxes))
    
    overall_boxes = np.concatenate(overall_boxes, axis=0)
    #print(overall_boxes)
    overall_boxes = overall_boxes[overall_boxes[:, 1].argsort()[::-1]]
    boxes = overall_boxes[:, 2:]
    scores = overall_boxes[:, 1]
    labels = overall_boxes[:, 0]
    result = []
    for info in overall_boxes:
        reform = {}
        #print(info[1])
        reform['sample_token'] = info[6]
        reform['translation'] = info[2]
        reform['size'] = info[3]
        reform['rotation'] = info[4]
        reform['velocity'] = info[5]
        reform['detection_name'] = info[0]
        reform['detection_score'] = info[1]
        reform['attribute_name'] = info[7]
        result.append(reform)
    
    
    if len(result)>=500:
        print('too long!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        score_temp = []
        for box_info in result:
            score_temp.append(box_info['detection_score'])
        score = np.array(score_temp)
        idxs = np.argsort(score)
        indices = idxs[len(idxs)-500:]
        temp = []
        for indice in indices:
            temp.append(result[indice])
        print('len',len(temp))
        return temp
    return result


# In[9]:


def prefilter_boxes(boxes_info,skip_thr):
        new_boxes = dict()
        num_eliminate = 0
        for t in range(len(boxes_info)):
            for box_info in boxes_info[t]:
                b = box_info.copy()
                if box_info['detection_score'] <= skip_thr:
                    num_eliminate = num_eliminate +1
                    continue
                    
                label = box_info['detection_name']

                if label not in new_boxes:
                    new_boxes[label] = []
                new_boxes[label].append(b)
        print('skip numbers',num_eliminate)
        return new_boxes
    
def non_max_suppression_fast_3d_class(object_list,overlapThresh,skip_thr):
    
    nms_result = []
    filtered_boxes = prefilter_boxes(object_list,skip_thr)
    for label in filtered_boxes:
        filtered_boxes_class = filtered_boxes[label]
        temp = []
        score_temp = []
        for box_info in filtered_boxes_class:
            box_points = box_center_to_corner(box_info['translation'],box_info['size'],box_info['rotation'])
            temp.append(box_points)
            score_temp.append(box_info['detection_score'])
        boxes = np.array(temp)
        scores = np.array(score_temp)
        pick = []
        # compute the area of the bounding boxes and sort the bounding
        # boxes by the bottom-right y-coordinate of the bounding box
        idxs = np.argsort(scores)

        while len(idxs) > 0:

            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]

            b1 = torch.Tensor(np.array([boxes[i]]))
            b2 = torch.Tensor(np.array(boxes[idxs[:last]]))

            if len(b2)<=0:
                break
            overlap_vol,iou = box3d_overlap(b1, b2)
            pick.append(i)
            idxs = np.delete(idxs, np.concatenate(([last],np.where(iou > overlapThresh)[1])))
        
        for i in pick:
            nms_result.append(filtered_boxes_class[i])
        #print('len nms single',len(pick))
    #nms_result = np.concatenate(nms_result, axis=0)   
    print('len nms result',len(nms_result))
    
    if len(nms_result)>=500:
        print('too long!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        score_temp = []
        for box_info in nms_result:
            score_temp.append(box_info['detection_score'])
        score = np.array(score_temp)
        idxs = np.argsort(score)
        indices = idxs[len(idxs)-500:]
        temp = []
        for indice in indices:
            temp.append(nms_result[indice])
        print('len',len(temp))
        
        return temp
        
    return nms_result
    


# In[26]:


def minimize_bbox_numbers(result):
    print('too long!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    score_temp = []
    for box_info in result:
        score_temp.append(box_info['detection_score'])
    score = np.array(score_temp)
    idxs = np.argsort(score)
    indices = idxs[len(idxs)-500:]
    temp = []
    for indice in indices:
        temp.append(result[indice])
    print('len',len(temp))
    
    return temp


# In[27]:


#Fusion based on center distance
# input boxes_info,threhold of distence
# output index box[index] score[index]
def center_distence_class(object_list,overlapThresh,skip_thr): #pair the BBox with center distence
    filtered_boxes = prefilter_boxes(object_list,skip_thr)
    cd_result = []
    for label in filtered_boxes:
        boxes_info = filtered_boxes[label]
        xx = []
        yy = []
        zz = []
        #print('test',boxes_info)
        scores = []
        for box_info in boxes_info:
            xx.append(box_info['translation'][0])
            yy.append(box_info['translation'][1])
            zz.append(box_info['translation'][2])
            scores.append(box_info['detection_score'])
        score = np.array(scores)
        x = np.array(xx)
        y = np.array(yy)
        z = np.array(zz)
        bbox = np.array(boxes_info)
        pick = []
        #print('cam + lidar = ', len(boxes_info))
        idxs = np.argsort(score)
        #print(idxs)
        while len(idxs) > 0:

            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.array(x[idxs[:last]])
            yy1 = np.array(y[idxs[:last]])
            zz1 = np.array(z[idxs[:last]])
            xx2 = np.array(x[i])
            yy2 = np.array(y[i])
            zz2 = np.array(z[i])
            # compute the distance of the bounding box
            dis = np.sqrt((xx1-xx2)**2+(yy1-yy2)**2+(zz1-zz2)**2)
            #print('dis:  ',dis)
            # compute the ratio of overlap
            #print(overlap)
            # delete all indexes from the index list that have

            pick.append(i)
            idxs = np.delete(idxs, np.concatenate(([last],np.where(dis <= overlapThresh)[0])))
            # return only the bounding boxes that were picked
        #print('len',len(bbox[pick]))
        
        for indice in pick:
            cd_result.append(boxes_info[indice])
    
    if len(cd_result) >500:
        print('too many boxes -----> minimize to 500 boxes')
        cd_result = minimize_bbox_numbers(cd_result)
    print('final len:',len(cd_result))    
    return cd_result

'''
# In[40]:


detector = 'lidar_mini_val'
detector = 'camera_mini_val'
detector = 'pointpillars_secfpn_mini_val'
detector = 'centerpoint_voxel_circlenms_mini_val'
detector = 'fcos_mini_val'
result,frames = load_detector(detector)
for frame in frames:
    boxes_info = []
    boxes_info.append(result[frame])
    nms_result = non_max_suppression_fast_3d_class(boxes_info,0.425,0.099) 
    result[frame] = nms_result
name = 'pre_nms/fcos'
create_and_save_json(result,name)


# In[16]:



#WBF
#cd
#NMS
detector_1 = 'lidar_mini_val'
detector_2 = 'camera_mini_val'
detector_3 = 'centerpoint_voxel_circlenms_mini_val'
detector_4 = 'pointpillars_secfpn_mini_val'
detector_5 = 'fcos_mini_val'


result_camera,result_lidar,frames = load_mini_val(detector_3,detector_5)

time_start = time.time() #start clock

c = 1
result={}
for frame in frames[:]:

    #Prepare box info
    print('frame: ',c)
    c = c+1
    object_list = []
    object_list_lidar = result_lidar[frame]
    object_list_camera = result_camera[frame]
    print(detector_2,' len:',len(object_list_lidar),detector_1,' len:',len(object_list_camera),'len original: ',len(object_list_lidar)+len(object_list_camera))
    for box in object_list_lidar:
        object_list.append(box)
    for box in object_list_camera:
        object_list.append(box)

    '''
'''
    #WBF
    print('WBF Fusion')
    boxes_info = []
    boxes_info.append(object_list_camera)
    boxes_info.append(object_list_lidar)
    wbf_result = weighted_boxes_fusion_3d(boxes_info,weights = [1,1],iou_thr=0.1, skip_box_thr=0.099,intersection=True)
    print('wbf len',len(wbf_result))
    result[frame]=wbf_result
    
    
    #NMS
    print("NMS")
    boxes_info = []
    boxes_info.append(object_list_camera)
    boxes_info.append(object_list_lidar)
    nms_result = non_max_suppression_fast_3d_class(boxes_info,0.425,0.099) 
    result[frame] = nms_result
    

    #center distance
    print('Center distance')
    boxes_info = []
    boxes_info.append(object_list_camera)
    boxes_info.append(object_list_lidar)
    cd_result = center_distence_class(boxes_info,1,0.099) #object_list,overlapThresh,
    
    
    
    
    result[frame] = cd_result
        
    

time_end = time.time()    #end clock
time_c= time_end - time_start   #time cost
print('time cost', time_c, 's')
name = 'pre_nms/pointpillar'
create_and_save_json(result,name)


# In[ ]:





# In[34]:


# Union CD parameter testing
detector_1 = 'lidar_mini_val'
detector_2 = 'camera_mini_val'
detector_3 = 'centerpoint_voxel_circlenms_mini_val'
detector_4 = 'pointpillars_secfpn_mini_val'
detector_5 = 'fcos_mini_val'
result_camera,result_lidar,frames = load_mini_val(detector_2,detector_5)
result = {}
result_path = []
result_index = []
for i in range(24):
    for frame in frames[:]:
        object_list = []
        object_list_lidar = result_lidar[frame]
        object_list_camera = result_camera[frame]
        print(detector_2,' len:',len(object_list_lidar),detector_1,' len:',len(object_list_camera),'len original: ',len(object_list_lidar)+len(object_list_camera))
        boxes_info = []
        boxes_info.append(object_list_camera)
        boxes_info.append(object_list_lidar)
        cd_result = center_distence_class(boxes_info,round(0.2+0.2*i,2),0.099) #object_list,overlapThresh,skip_thr
        result[frame] = cd_result
    index = 'D2_D5_' + str(round(0.2+0.2*i,2))
    name = 'CD/threshold/Union/'+index
    path = '/Users/mike/Desktop/data/sets/out_put_dir/'+name +'.json'
    
    result_path.append(path)
    result_index.append(index)
    create_and_save_json(result,name)
print(result_path)
print(result_index)


# In[ ]:

'''


