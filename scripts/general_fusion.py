#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import torch
import math
import numpy as np
from collections import OrderedDict
from pytorch3d.ops.iou_box3d import box3d_overlap
from ensemble_boxes import *
import time
import os


# In[2]:


# Copyright (c) Meta Platforms, Inc. and affiliates.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree.
# 
from typing import Tuple

import torch
import torch.nn.functional as F
from pytorch3d import _C
from torch.autograd import Function


# -------------------------------------------------- #
#                  CONSTANTS                         #
# -------------------------------------------------- #
"""
_box_planes and _box_triangles define the 4- and 3-connectivity
of the 8 box corners.
_box_planes gives the quad faces of the 3D box
_box_triangles gives the triangle faces of the 3D box
"""
_box_planes = [
    [0, 1, 2, 3],
    [3, 2, 6, 7],
    [0, 1, 5, 4],
    [0, 3, 7, 4],
    [1, 2, 6, 5],
    [4, 5, 6, 7],
]
_box_triangles = [
    [0, 1, 2],
    [0, 3, 2],
    [4, 5, 6],
    [4, 6, 7],
    [1, 5, 6],
    [1, 6, 2],
    [0, 4, 7],
    [0, 7, 3],
    [3, 2, 6],
    [3, 6, 7],
    [0, 1, 5],
    [0, 4, 5],
]


def _check_coplanar(boxes: torch.Tensor, eps: float = 1e-4) -> None:
    faces = torch.tensor(_box_planes, dtype=torch.int64, device=boxes.device)
    verts = boxes.index_select(index=faces.view(-1), dim=1)
    B = boxes.shape[0]
    P, V = faces.shape
    # (B, P, 4, 3) -> (B, P, 3)
    v0, v1, v2, v3 = verts.reshape(B, P, V, 3).unbind(2)

    # Compute the normal
    e0 = F.normalize(v1 - v0, dim=-1)
    e1 = F.normalize(v2 - v0, dim=-1)
    normal = F.normalize(torch.cross(e0, e1, dim=-1), dim=-1)

    # Check the fourth vertex is also on the same plane
    mat1 = (v3 - v0).view(B, 1, -1)  # (B, 1, P*3)
    mat2 = normal.view(B, -1, 1)  # (B, P*3, 1)
    if not (mat1.bmm(mat2).abs() < eps).all().item():
        msg = "Plane vertices are not coplanar!!!"
        return 0
        #raise ValueError(msg)

    return


def _check_nonzero(boxes: torch.Tensor, eps: float = 1e-4) -> None:
    """
    Checks that the sides of the box have a non zero area
    """
    faces = torch.tensor(_box_triangles, dtype=torch.int64, device=boxes.device)
    verts = boxes.index_select(index=faces.view(-1), dim=1)
    B = boxes.shape[0]
    T, V = faces.shape
    # (B, T, 3, 3) -> (B, T, 3)
    v0, v1, v2 = verts.reshape(B, T, V, 3).unbind(2)

    normals = torch.cross(v1 - v0, v2 - v0, dim=-1)  # (B, T, 3)
    face_areas = normals.norm(dim=-1) / 2

    if (face_areas < eps).any().item():
        msg = "Planes have zero areas"
        raise ValueError(msg)

    return


class _box3d_overlap(Function):
    """
    Torch autograd Function wrapper for box3d_overlap C++/CUDA implementations.
    Backward is not supported.
    """

    @staticmethod
    def forward(ctx, boxes1, boxes2):
        """
        Arguments defintions the same as in the box3d_overlap function
        """
        vol, iou = _C.iou_box3d(boxes1, boxes2)
        return vol, iou

    @staticmethod
    def backward(ctx, grad_vol, grad_iou):
        raise ValueError("box3d_overlap backward is not supported")


def box3d_overlap(
    boxes1: torch.Tensor, boxes2: torch.Tensor, eps: float = 1e-4
) -> Tuple[torch.Tensor, torch.Tensor]:
    """
    Computes the intersection of 3D boxes1 and boxes2.
    Inputs boxes1, boxes2 are tensors of shape (B, 8, 3)
    (where B doesn't have to be the same for boxes1 and boxes2),
    containing the 8 corners of the boxes, as follows:
        (4) +---------+. (5)
            | ` .     |  ` .
            | (0) +---+-----+ (1)
            |     |   |     |
        (7) +-----+---+. (6)|
            ` .   |     ` . |
            (3) ` +---------+ (2)
    NOTE: Throughout this implementation, we assume that boxes
    are defined by their 8 corners exactly in the order specified in the
    diagram above for the function to give correct results. In addition
    the vertices on each plane must be coplanar.
    As an alternative to the diagram, this is a unit bounding
    box which has the correct vertex ordering:
    box_corner_vertices = [
        [0, 0, 0],
        [1, 0, 0],
        [1, 1, 0],
        [0, 1, 0],
        [0, 0, 1],
        [1, 0, 1],
        [1, 1, 1],
        [0, 1, 1],
    ]
    Args:
        boxes1: tensor of shape (N, 8, 3) of the coordinates of the 1st boxes
        boxes2: tensor of shape (M, 8, 3) of the coordinates of the 2nd boxes
    Returns:
        vol: (N, M) tensor of the volume of the intersecting convex shapes
        iou: (N, M) tensor of the intersection over union which is
            defined as: `iou = vol / (vol1 + vol2 - vol)`
    """
    if not all((8, 3) == box.shape[1:] for box in [boxes1, boxes2]):
        raise ValueError("Each box in the batch must be of shape (8, 3)")

    _check_coplanar(boxes1, eps)
    _check_coplanar(boxes2, eps)
    _check_nonzero(boxes1, eps)
    _check_nonzero(boxes2, eps)

    # pyre-fixme[16]: `_box3d_overlap` has no attribute `apply`.
    vol, iou = _box3d_overlap.apply(boxes1, boxes2)

    return vol, iou


# In[3]:


def load_mini_val(detector1,detector2):
    path_1 = '/Users/mike/Desktop/MasterThesis/'+ detector1 +'.json'
    path_2 = '/Users/mike/Desktop/MasterThesis/'+ detector2 +'.json'
    if not os.path.isfile(path_1):
        print(path_1)
        raise ValueError('Detector 1 not found')
        
    if not os.path.isfile(path_2):
        raise ValueError('Detector 2 not found')
    with open(path_1) as f:
        data_camera = json.load(f)
    #print(type(data_camera))
    with open(path_2) as d:
        data_lidar = json.load(d)
    #print(data_camera['meta'])
    print(data_lidar['meta'])
    frame_eval =['3e8750f331d7499e9b5123e9eb70f2e2', '3950bd41f74548429c0f7700ff3d8269', 'c5f58c19249d4137ae063b0e9ecd8b8e'
             , '700c1a25559b4433be532de3475e58a9', '747aa46b9a4641fe90db05d97db2acea', 'f4f86af4da3b49e79497deda5c5f223a'
             , '6832e717621341568c759151b5974512', 'c59e60042a8643e899008da2e446acca', 'fa65a298c01f44e7a182bbf9e5fe3697'
             , 'a98fba72bde9433fb882032d18aedb2e', 'b6b0d9f2f2e14a3aaa2c8aedeb1edb69', '796b1988dd254f74bf2fb19ba9c5b8c6'
             , '0d0700a2284e477db876c3ee1d864668', 'de7593d76648450e947ba0c203dee1b0', 'f753d3f87e5b40af87ff2cbf7c8e7082'
             , 'b22fa0b3c34f47b6a360b60f35d5d567', '0a0d6b8c2e884134a3b48df43d54c36a', '8e9c2cba0ee74056aa3746e8391d54a9'
             , 'fdc39b23ab4242eda6ec5e1e6574fe33', '9ee4020153674b9e9943d395ff8cfdf3', '5b03af7a953245b5a3b23191ed4da62a'
             , 'd0ecf474e0e64950aa265cd44b9c9d75', '456ec36cb4a44ca78f36fbd90c0c34fa', '6bfd42cf0aba4f1a94ec11fa43e2dd92'
             , '0af0feb5b1394b928dd13d648de898f5', '61d9340c5ad8418dafe7a4af1b96e6b9', '9e7683e8586542a1b6032980c45f15ce'
             , 'cc57c1ea80fe46a7abddfdb15654c872', '4cf5d6c3f6ab42aab23f67b5a9782d1a', '6d9984d09d52479e837da2fd09e192cc'
             , '38a28a3aaf2647f2a8c0e90e31267bf8', '6402fd1ffaf041d0b9162bd92a7ba0a2', 'ce94ef7a0522468e81c0e2b3a2f1e12d'
             , '9fcdc52b791045e99c623c5fc643331f', '1c9a906c40f6417bbe1cea06d6e55501', 'a771effa2a2648d78096c3e92b95b129'
             , 'b9ea04a6121d4a8bb00199b885aa5ef0', '87e772078a494d42bd34cd16172808bc', 'f40544fd4f5d42abbcfa948eeaf86850'
             , '281b92269fd648d4b52d06ac06ca6d65', 'b5989651183643369174912bc5641d3b', '0bb62a68055249e381b039bf54b0ccf8'
             , '07fad91090c746ccaa1b2bdb55329e20', 'ae5004bf4ebb4db0a84cb3c27bd398d1', 'ac8b4d49731d43289579f014cb7c97ef'
             , 'c1eed31234b94e9f8e22fbf3428b0ac2', 'edba7d0cf1c64d2a9c8a5ebddab507a9', 'a5afebb0aa5e4d7c95665788ce51ec58'
             , '5b7cb170eee6468aa1fdbd3abcf63c5a', 'd8251bbc2105497ab8ec80827d4429aa', '048a45dd2cf54aa5808d8ccc85731d44'
             , '858a1ece22cf45d9bc71e42336604b78', '372725a4b00e49c78d6d0b1c4a38b6e0', '61a7bd24f88a46c2963280d8b13ac675'
             , '609d5177362340458a3bfd4949cd1e64', 'b1303058fa624645b753d7283d70de45', 'a19a80c905674faab7203a3a4e0f5246'
             , 'b6c420c3a5bd4a219b1cb82ee5ea0aa7', '8092909473464f80b9f791a4d31ddcb8', 'e174cb43655f49dab7ffa27b973670e3'
             , '6ff9723a60bf4e14b328b3b19f04dc32', '06be0e3b665c44fa8d17d9f4770bdf9c', '9150678870764c1b87a649a25939c61b'
             , '8573a885a7cb41d185c05029eeb9a54e', '55c258972acb4300a3a6077a531ab050', '44237858a539457da65822bfcf58c414'
             , '8cd36e9531fb4eba8e6ac1d666c4641c', '7bebe3c9be714f02837f8617c56df122', '4b894442c95141f9affd731d9da7b43c'
             , 'e00dc15130dc44e796687baadd076ae4', 'e63b83b436a0479db7362338cdfab118', 'c567f9b8e9f34817acdc9c49d791c557'
             , '8e352d4a6c6f40c8ad4f10a3d2c3f158', '841dd6709e9b4d7c9d6bf888f1fe6d7e', 'e54b784a608644a2804fcf800c7499f7'
             , 'a1289b27ca1d41deb6fc982be9a3d03c', 'e6e877f31dd447199b56cae07f86daad', 'cb4e6195faad467094fbd4d0a9e960e9'
             , '7f594234e8034228b1a7d727f1981e09', '9cdbf5ff7f294549aea0a4307e5d104a', 'b4ff30109dd14c89b24789dc5713cf8c']
    result_camera = data_camera['results']
    result_lidar = data_lidar['results']
    frame_camera = []
    frame_lidar = []
    frames = []
    #print (result_camera.keys())
    for i in result_camera.keys():
        frame_camera.append(i)
    for j in result_lidar.keys():
        frame_lidar.append(j)
    print (type(frame_lidar))
    frames = frame_camera
    print ('frame camera',len(frame_camera))
    print ('frame lidar',len(frame_lidar))
    return(result_camera,result_lidar,frames)


# In[4]:


#provide the eight corners in space XYZ
#input box_info
#output shape (8,3)
def box_center_to_corner(translation,size,rotation):
    #translation = box_info['translation']
    #size = box_info['size']
    #rotation = box_info['rotation']
    translation = translation
    size = size
    rotation = rotation
    # To return
    corner_boxes = np.zeros((8, 3))

    translation = translation
    h, w, l = size[0], size[1], size[2]
    W,X,Y,Z = rotation

    # Create a bounding box outline
    bounding_box = np.array([
        [-l/2, -l/2, l/2, l/2, -l/2, -l/2, l/2, l/2],
        [w/2, -w/2, -w/2, w/2, w/2, -w/2, -w/2, w/2],
        [-h/2, -h/2, -h/2, -h/2, h/2, h/2, h/2, h/2]])

    # Standard 3x3 rotation matrix around the Z axis
    rotation_matrix = quatRotation(W,X,Y,Z)
    #rotation_matrix = rotation_matrix[:3]
    #print(rotation_matrix)
    # Repeat the [x, y, z] eight times
    eight_points = np.tile(translation, (8, 1))

    # Translate the rotated bounding box by the
    # original center position to obtain the final box
    corner_box = np.dot(
        rotation_matrix, bounding_box) + eight_points.transpose()

    return corner_box.transpose()


# In[5]:


#Return a transfer matrics
#input Quaternion
#output 3X3 matrix
def quatRotation(W,X,Y,Z):
    """
    Covert a quaternion into a full three-dimensional rotation matrix.
 
    Input
    :param Q: A 4 element array representing the quaternion (q0,q1,q2,q3) 
 
    Output
    :return: A 3x3 element matrix representing the full 3D rotation matrix. 
             This rotation matrix converts a point in the local reference 
             frame to a point in the global reference frame.
    """
    # Extract the values from Q
    q0 = math.cos(W/2)
    q1 = X*math.sin(W/2)
    q2 = Y*math.sin(W/2)
    q3 = Z*math.sin(W/2)
     
    # First row of the rotation matrix
    r00 = 2 * (q0 * q0 + q1 * q1) - 1
    r01 = 2 * (q1 * q2 - q0 * q3)
    r02 = 2 * (q1 * q3 + q0 * q2)
     
    # Second row of the rotation matrix
    r10 = 2 * (q1 * q2 + q0 * q3)
    r11 = 2 * (q0 * q0 + q2 * q2) - 1
    r12 = 2 * (q2 * q3 - q0 * q1)
     
    # Third row of the rotation matrix
    r20 = 2 * (q1 * q3 - q0 * q2)
    r21 = 2 * (q2 * q3 + q0 * q1)
    r22 = 2 * (q0 * q0 + q3 * q3) - 1
     
    # 3x3 rotation matrix
    mat = np.array([[r00, r01, r02],
                           [r10, r11, r12],
                           [r20, r21, r22]])
                            
    return mat


# In[6]:


#save as json file
#meta = {'use_camera': True, 'use_lidar': False,'use_radar': False, 'use_map': False, 'use_external': False}
#results = data
def create_and_save_json(data,name):
    meta = {'use_camera': True, 'use_lidar': False,
                     'use_radar': False, 'use_map': False, 'use_external': False}
    json_file = {"meta":meta}
    json_file['results'] = data
    path = "/Users/mike/Desktop/data/sets/out_put_dir/"+name +".json"
    out_file = open(path, "w")
  
    json.dump(json_file, out_file)
    
    out_file.close()


# In[36]:


#create mini_val_set 
#save time for testing
def create_mini_val_set(path_name,name):
    with open('/Users/mike/Desktop/data/sets/out_put_dir/'+path_name) as f:
        data = json.load(f)
    frames = []
    result = {}
    data_results = data['results']
    frame_eval =['3e8750f331d7499e9b5123e9eb70f2e2', '3950bd41f74548429c0f7700ff3d8269', 'c5f58c19249d4137ae063b0e9ecd8b8e'
             , '700c1a25559b4433be532de3475e58a9', '747aa46b9a4641fe90db05d97db2acea', 'f4f86af4da3b49e79497deda5c5f223a'
             , '6832e717621341568c759151b5974512', 'c59e60042a8643e899008da2e446acca', 'fa65a298c01f44e7a182bbf9e5fe3697'
             , 'a98fba72bde9433fb882032d18aedb2e', 'b6b0d9f2f2e14a3aaa2c8aedeb1edb69', '796b1988dd254f74bf2fb19ba9c5b8c6'
             , '0d0700a2284e477db876c3ee1d864668', 'de7593d76648450e947ba0c203dee1b0', 'f753d3f87e5b40af87ff2cbf7c8e7082'
             , 'b22fa0b3c34f47b6a360b60f35d5d567', '0a0d6b8c2e884134a3b48df43d54c36a', '8e9c2cba0ee74056aa3746e8391d54a9'
             , 'fdc39b23ab4242eda6ec5e1e6574fe33', '9ee4020153674b9e9943d395ff8cfdf3', '5b03af7a953245b5a3b23191ed4da62a'
             , 'd0ecf474e0e64950aa265cd44b9c9d75', '456ec36cb4a44ca78f36fbd90c0c34fa', '6bfd42cf0aba4f1a94ec11fa43e2dd92'
             , '0af0feb5b1394b928dd13d648de898f5', '61d9340c5ad8418dafe7a4af1b96e6b9', '9e7683e8586542a1b6032980c45f15ce'
             , 'cc57c1ea80fe46a7abddfdb15654c872', '4cf5d6c3f6ab42aab23f67b5a9782d1a', '6d9984d09d52479e837da2fd09e192cc'
             , '38a28a3aaf2647f2a8c0e90e31267bf8', '6402fd1ffaf041d0b9162bd92a7ba0a2', 'ce94ef7a0522468e81c0e2b3a2f1e12d'
             , '9fcdc52b791045e99c623c5fc643331f', '1c9a906c40f6417bbe1cea06d6e55501', 'a771effa2a2648d78096c3e92b95b129'
             , 'b9ea04a6121d4a8bb00199b885aa5ef0', '87e772078a494d42bd34cd16172808bc', 'f40544fd4f5d42abbcfa948eeaf86850'
             , '281b92269fd648d4b52d06ac06ca6d65', 'b5989651183643369174912bc5641d3b', '0bb62a68055249e381b039bf54b0ccf8'
             , '07fad91090c746ccaa1b2bdb55329e20', 'ae5004bf4ebb4db0a84cb3c27bd398d1', 'ac8b4d49731d43289579f014cb7c97ef'
             , 'c1eed31234b94e9f8e22fbf3428b0ac2', 'edba7d0cf1c64d2a9c8a5ebddab507a9', 'a5afebb0aa5e4d7c95665788ce51ec58'
             , '5b7cb170eee6468aa1fdbd3abcf63c5a', 'd8251bbc2105497ab8ec80827d4429aa', '048a45dd2cf54aa5808d8ccc85731d44'
             , '858a1ece22cf45d9bc71e42336604b78', '372725a4b00e49c78d6d0b1c4a38b6e0', '61a7bd24f88a46c2963280d8b13ac675'
             , '609d5177362340458a3bfd4949cd1e64', 'b1303058fa624645b753d7283d70de45', 'a19a80c905674faab7203a3a4e0f5246'
             , 'b6c420c3a5bd4a219b1cb82ee5ea0aa7', '8092909473464f80b9f791a4d31ddcb8', 'e174cb43655f49dab7ffa27b973670e3'
             , '6ff9723a60bf4e14b328b3b19f04dc32', '06be0e3b665c44fa8d17d9f4770bdf9c', '9150678870764c1b87a649a25939c61b'
             , '8573a885a7cb41d185c05029eeb9a54e', '55c258972acb4300a3a6077a531ab050', '44237858a539457da65822bfcf58c414'
             , '8cd36e9531fb4eba8e6ac1d666c4641c', '7bebe3c9be714f02837f8617c56df122', '4b894442c95141f9affd731d9da7b43c'
             , 'e00dc15130dc44e796687baadd076ae4', 'e63b83b436a0479db7362338cdfab118', 'c567f9b8e9f34817acdc9c49d791c557'
             , '8e352d4a6c6f40c8ad4f10a3d2c3f158', '841dd6709e9b4d7c9d6bf888f1fe6d7e', 'e54b784a608644a2804fcf800c7499f7'
             , 'a1289b27ca1d41deb6fc982be9a3d03c', 'e6e877f31dd447199b56cae07f86daad', 'cb4e6195faad467094fbd4d0a9e960e9'
             , '7f594234e8034228b1a7d727f1981e09', '9cdbf5ff7f294549aea0a4307e5d104a', 'b4ff30109dd14c89b24789dc5713cf8c']
    for frame in frame_eval:
         result[frame] = data_results[frame]
    create_and_save_json(result,'baseline/'+name)
    


# In[37]:









# In[8]:


#WBF
def prefilter_boxes(boxes_info,weights,thr):
    new_boxes = dict()
    for t in range(len(boxes_info)):
        for box_info in boxes_info[t]:
            #print('box_info',box_info)
            
            b = box_info.copy()
            label = box_info['detection_name']
            score = box_info['detection_score']
            if score < thr:
                continue
            translation = box_info['translation']
            size = box_info['size']
            rotation = box_info['rotation']
            velocity = box_info['velocity']
            sample_token = box_info['sample_token']
            attribute_name = box_info['attribute_name']
            b = [label,score*weights[t],translation,size,rotation,velocity,sample_token,attribute_name]
            if label not in new_boxes:
                new_boxes[label] = []
            new_boxes[label].append(b)
    for k in new_boxes:
        current_boxes = np.array(new_boxes[k],dtype=object)
        new_boxes[k] = current_boxes[current_boxes[:,1].argsort()[::-1]]
        #print('score org',new_boxes[k][:,1])
    #print(new_boxes)
    return new_boxes
def bb_intersection_over_union_3d(A,B):
    t_a,s_a,r_a = A
    t_b,s_b,r_b = B
    Box_A = box_center_to_corner(t_a,s_a,r_a)
    Box_B = box_center_to_corner(t_b,s_b,r_b)

    overlap_vol,iou = box3d_overlap(torch.Tensor([Box_A]), torch.Tensor([Box_B]))
        
    return np.array(iou)    

def get_weighted_box(boxes, conf_type='max'):
    print('get_weighted_box')
    box = np.array(['',0,[],[],[],[],'',''],dtype = object)
    #box = np.zeros(8, dtype=np.float32)
    conf = 0
    conf_list = []
    points = np.zeros((8,3))
    size = np.zeros(3)
    velocity = np.zeros(2)
    for b in boxes:
        
        pts = box_center_to_corner(b[2],b[3],b[4])
        #print(pts.shape)
        #print('b[1]',b[1])
        points = points + (pts*float(b[1]))
        #print('b[3]',b[3])
        size = size + ( np.array(b[3])*float(b[1]))
        velocity = velocity +(np.array(b[5])*float(b[1]))
        conf += b[1]
        conf_list.append(b[1])
    #print('b[1]',conf_list)
    box[0] = boxes[0][0]
    box[4] = boxes[0][4]
    box[6] = boxes[0][6]
    box[7] = boxes[0][7]
    if conf_type == 'avg':
        box[1] = conf / len(boxes)
    elif conf_type == 'max':
        #print('score',conf_list)
        box[1] = np.array(conf_list).max()
        #print('score max',np.array(conf_list).max())
    points /= conf
    size/=conf
    #print('*****',size)
    velocity/=conf
    translation = np.sum(points,axis=0)/8
    box[2] = translation.tolist()
    box[3] = size.tolist()
    ###
    #quaternion
    ###
    box[5] = velocity.tolist()

    return box
    
def find_matching_box(boxes_list, new_box, match_iou):


    best_iou = match_iou
    best_index = -1
    for i in range(len(boxes_list)):
        box = boxes_list[i]
        if box[0] != new_box[0]:
            continue
        iou = bb_intersection_over_union_3d(box[2:5], new_box[2:5])
        if iou > best_iou:
            #print('iou',iou)
            best_index = i
            best_iou = iou


    return best_index, best_iou
def weighted_boxes_fusion_3d(boxes_info, weights=None, iou_thr=0.55, skip_box_thr=0.0,intersection=False, conf_type='max', allows_overflow=False):
    if intersection == True:
        print('Intersection calculating')
    if weights is None:
        weights = np.ones(len(boxes_list))
    if len(weights) != len(boxes_info):
        print('Warning: incorrect number of weights {}. Must be: {}. Set weights equal to 1.'.format(len(weights), len(boxes_list)))
        weights = np.ones(len(boxes_info))
    weights = np.array(weights)

    if conf_type not in ['avg', 'max']:
        print('Error. Unknown conf_type: {}. Must be "avg" or "max". Use "avg"'.format(conf_type))
        conf_type = 'avg'

    filtered_boxes = prefilter_boxes(boxes_info, weights, skip_box_thr)
    if len(filtered_boxes) == 0:
        print('Warning: filtered boxes not found')
        return np.zeros((0, 6)), np.zeros((0,)), np.zeros((0,))

    overall_boxes = []
    for label in filtered_boxes:
        boxes = filtered_boxes[label]
        new_boxes = []
        weighted_boxes = []
        intersection_index = []
        # Clusterize boxes
        for j in range(0, len(boxes)):
            index, best_iou = find_matching_box(weighted_boxes, boxes[j], iou_thr)
            if index != -1:
                new_boxes[index].append(boxes[j])
                weighted_boxes[index] = get_weighted_box(new_boxes[index], conf_type)
                #print('index',index)
            else:
                new_boxes.append([boxes[j].copy()])
                weighted_boxes.append(boxes[j].copy())
            #print('new_boxes, wb',len(new_boxes),len(weighted_boxes),len(intersection_index))
        
        #reset score
        if intersection == True:
            for i in range(len(new_boxes)):
                if len(new_boxes[i])>1:
                    intersection_index.append(i)
                    
            if intersection_index == []:
                break
            weighted_boxes=[weighted_boxes[x] for x in intersection_index]
            new_boxes=[new_boxes[x] for x in intersection_index]
        #print('inter',intersection_index,weighted_boxes)   
        #print('n',len(new_boxes[0]))
            
        for i in range(len(new_boxes)):
            if not allows_overflow:
                weighted_boxes[i][1] = weighted_boxes[i][1] * min(weights.sum(), len(new_boxes[i])) / weights.sum()
            else:
                weighted_boxes[i][1] = weighted_boxes[i][1] * len(new_boxes[i]) / weights.sum()
        overall_boxes.append(np.array(weighted_boxes))
    
    overall_boxes = np.concatenate(overall_boxes, axis=0)
    #print(overall_boxes)
    overall_boxes = overall_boxes[overall_boxes[:, 1].argsort()[::-1]]
    boxes = overall_boxes[:, 2:]
    scores = overall_boxes[:, 1]
    labels = overall_boxes[:, 0]
    temp = []
    for info in overall_boxes:
        reform = {}
        #print(info[1])
        reform['sample_token'] = info[6]
        reform['translation'] = info[2]
        reform['size'] = info[3]
        reform['rotation'] = info[4]
        reform['velocity'] = info[5]
        reform['detection_name'] = info[0]
        reform['detection_score'] = info[1]
        reform['attribute_name'] = info[7]
        temp.append(reform)
    return temp


# In[9]:


#find the overlap box 
#input objects_list,threhold iou
#output dict{result}
def iou_fusion_keepcam(objects_lidar,objects_camera,thr):
    thr = thr
    temp_iou_list = []
    temp = []
    for box_info in objects_lidar:
        translation = box_info['translation']
        size = box_info['size']
        rotation = box_info['rotation']
        box_points = box_center_to_corner(translation,size,rotation)
        temp.append(box_points)
    boxes_lidar = np.array(temp)
    temp = []
    for box_info in objects_camera:
        translation = box_info['translation']
        size = box_info['size']
        rotation = box_info['rotation']
        box_points = box_center_to_corner(translation,size,rotation)
        temp.append(box_points)
    boxes_camera = np.array(temp)
    result = []
    iou_temp = []
    c = 0
    for i,box_lidar in enumerate(boxes_lidar):
        #print(boxes_lidar[i])
        b1 = torch.Tensor([box_lidar])
        for j,box_camera in enumerate(boxes_camera):
            b2 = torch.Tensor([box_camera])
            overlap_vol,iou = box3d_overlap(b1, b2)
            if iou>thr:         
                iou_temp.append([i,j,iou])
                c = c+1
                result.append(objects_camera[j])
    print(iou_temp)
    print(c)
    return result


# In[10]:


#Fusion based on IoU
def IoU_fusion(boxes,thr):
    overlapThresh = thr
    # Return an empty list, if no boxes given
    print(len(boxes))
    if len(boxes) == 0:
        return []
    print(type(boxes))
    #x1 = boxes[:, 0]  # x coordinate of the top-left corner
    # Compute the area of the bounding boxes and sort the bounding
    # Boxes by the bottom-right y-coordinate of the bounding box
    #areas = (x2 - x1 + 1) * (y2 - y1 + 1) # We add 1, because the pixel at the start as well as at the end counts
    # The indices of all boxes at start. We will redundant indices one by one.
    indices = np.arange(len(boxes))
    #print('o i',indices)
    for i,box in enumerate(boxes):
        # Create temporary indices  
        temp_indices = indices[indices!=i]
        # Find out the coordinates of the intersection box

        # Find out the width and the height of the intersection box
        # compute the ratio of overlap
        b2 = box
        box1 = torch.Tensor(boxes[temp_indices])
        box2 = torch.Tensor([b2])
        overlap_vol,iou = box3d_overlap(box1, box2)
        # if the actual boungding box has an overlap bigger than treshold with any other box, remove it's index  
        c = np.array(iou)
        #print('iou',len(c))
        if np.any(c > overlapThresh):
            #print('eliminate-------------------------------')
            indices = indices[indices != i]
            #for i in c:
             #   if i>overlapThresh:
                    #print('iou',i)
    
    #return only the boxes at the remaining indices
    return indices,boxes[indices]
                


# In[11]:


#Fusion based on center distance
# input boxes_info,threhold of distence
# output index box[index] score[index]
def center_distence(boxes_info,overlapThresh): #pair the BBox with center distence
    xx = []
    yy = []
    zz = []
    scores = []
    for box_info in boxes_info:
        xx.append(box_info['translation'][0])
        yy.append(box_info['translation'][1])
        zz.append(box_info['translation'][2])
        scores.append(box_info['detection_score'])
    score = np.array(scores)
    x = np.array(xx)
    y = np.array(yy)
    z = np.array(zz)
    bbox = np.array(boxes_info)
    pick = []
    print('cam + lidar = ', len(boxes_info))
    #print('x',x)
    #print('y',y)
    #print('z',z)
    idxs = np.argsort(score)
    #print(idxs)
    while len(idxs) > 0:

        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        
        #print('last,i: ',last,i)
        
        
        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.array(x[idxs[:last]])
        yy1 = np.array(y[idxs[:last]])
        zz1 = np.array(z[idxs[:last]])
        xx2 = np.array(x[i])
        yy2 = np.array(y[i])
        zz2 = np.array(z[i])

        # compute the width and height of the bounding box
        dis = np.sqrt((xx1-xx2)**2+(yy1-yy2)**2+(zz1-zz2)**2)
        #print('dis:  ',dis)
        # compute the ratio of overlap
        #print(overlap)
        # delete all indexes from the index list that have

        pick.append(i)
        idxs = np.delete(idxs, np.concatenate(([last],np.where(dis <= overlapThresh)[0])))
        # return only the bounding boxes that were picked
    print('len',len(bbox[pick]))
    cd_result = []
    for indice in pick:
        cd_result.append(object_list[indice])
    return cd_result


# In[12]:


def non_max_suppression_fast_3d(object_list,overlapThresh,skip_thr):
    
    temp = []
    score_temp = []
    for box_info in object_list:
        if box_info['detection_score']>skip_thr:
            box_points = box_center_to_corner(box_info['translation'],box_info['size'],box_info['rotation'])
            temp.append(box_points)
            score_temp.append(box_info['detection_score'])
    
    boxes = np.array(temp)
    #print('len_orginal',len(object_list))
    scores = np.array(score_temp)
    pick = []
    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    idxs = np.argsort(scores)

    while len(idxs) > 0:

        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        b1 = torch.Tensor(np.array([boxes[i]]))
        b2 = torch.Tensor(np.array(boxes[idxs[:last]]))            
        #b1 = torch.Tensor([boxes[i]])
        #b2 = torch.Tensor(boxes[idxs[:last]])

        if len(b2)<=0:
            break
        overlap_vol,iou = box3d_overlap(b1, b2)
        pick.append(i)
        idxs = np.delete(idxs, np.concatenate(([last],np.where(iou > overlapThresh)[1])))
            
    #print('pick',len(pick),pick)
    print('len',len(boxes[pick]))
    nms_result = []
    score_ = np.array(scores[pick])
    idxs_ = np.argsort(score_)
    if len(idxs_)>500:
        print('minimize to 500')
        indices = idxs_[len(idxs_)-500:]
    else:
        indices = idxs_[:]
    for indice in indices:
        nms_result.append(object_list[indice])
    # return only the bounding boxes that were picked
    return nms_result

#addition remain top 500
def addition(detector1,detector2,skip_thr):
    
    result_camera,result_lidar,frames = load_mini_val(detector1,detector2)
    result={}
    for frame in frames[:]:

        print('frame: ',frame)
        object_list = []
        object_list_lidar = result_lidar[frame]
        object_list_camera = result_camera[frame]
        temp = []
        score_temp = []
        print('len cam lid',len(object_list_lidar),len(object_list_camera))
        for box in object_list_lidar:
            if box['detection_score']>=skip_thr:
                object_list.append(box)
                    
        for box in object_list_camera:
            if box['detection_score']>=skip_thr:
                object_list.append(box)
        add = object_list.copy()
        if len(object_list)>500:
            add = []
            print('boxes more than 500')
            for box in object_list:
                score_temp.appned(box['detection_score'])
            idxs = np.argsort(score_temp)
            for i in idex[len(object_list)-500:]:
                add.append(object_list[i])
        print('final',len(add))
        result[frame] = add

    #print('idx',score[idxs])
    #print('indice',score[indices])
    return result
# In[13]:

def load_data(path):
    with open(path) as f:
        data = json.load(f)
    result = data['results']
    return result
#addition remain top 500
def addition_(detector,skip_thr):
    path = os.getcwd()
    result = load_data(path+'/dataset/combination/'+ detector +'.json')
    frames = []
    for i in result.keys():
        frames.append(i) 
    for frame in frames[:]:
        print('frame: ',frame)
        object_list = []
        object_list = result[frame]
        addition = []
        for box in object_list:
            if box['detection_score']>=skip_thr:
                addition.append(box)

        add = addition.copy()
        score_temp = []
        if len(addition)>500:
            add = []
            print('boxes more than 500')
            for box in addition:
                score_temp.append(box['detection_score'])
            idxs = np.argsort(score_temp)
            for i in idxs[len(addition)-500:]:
                add.append(addition[i])
        print('final',len(add))
        result[frame] = add

    #print('idx',score[idxs])
    #print('indice',score[indices])
    return result


def NMS_intersection(object_list_lidar,object_list_camera,overlapThresh):
    temp_lidar = []
    score_lidar = []
    pick = []
    skip_thr = 0.1
    for box_info_lidar in object_list_lidar:
        if box_info_lidar['detection_score']>=skip_thr:
            box_points_lidar = box_center_to_corner(box_info_lidar['translation'],box_info_lidar['size'],box_info_lidar['rotation'])
            temp_lidar.append(box_points_lidar)
            score_lidar.append(box_info_lidar['detection_score'])
    boxes_lidar = np.array(temp_lidar)
    
    temp_camera = []
    score_camera = []
    for box_info_camera in object_list_camera:
        if box_info_camera['detection_score']>=skip_thr:
            box_points_camera = box_center_to_corner(box_info_camera['translation'],box_info_camera['size'],box_info_camera['rotation'])
            temp_camera.append(box_points_camera)
            score_camera.append(box_info_camera['detection_score'])
    boxes_camera = np.array(temp_camera)
    
    idxs_camera = np.argsort(score_camera)
    idxs_lidar = np.argsort(score_lidar)
    
    while len(idxs_camera)>0 and len(idxs_lidar) >0:
        last = len(idxs_camera) - 1
        i = idxs_camera[last]
        b1 = torch.Tensor(np.array(boxes_lidar[idxs_lidar[:]]))
        b2 = torch.Tensor(np.array([boxes_camera[idxs_camera[last]]]))
        
        #b1 = torch.Tensor(boxes_lidar[idxs_lidar[:]])
        #b2 = torch.Tensor([boxes_camera[idxs_camera[last]]])
        #print(b1.shape,b2.shape)
        if len(b1)<=0:
            break
        overlap_vol,iou = box3d_overlap(b1, b2)
        if np.any(np.where(iou>overlapThresh)):
            
            pick.append(i)
        idxs_lidar = np.delete(idxs_lidar, (np.where(iou > overlapThresh)[1]))
        idxs_camera = np.delete(idxs_camera, [last])
    #print('pick',len(pick))
    result = []
    for indice in pick:
        result.append(object_list_camera[indice])
    print('len',len(pick))
    return result


# In[14]:


def cd_intersection(object_list_lidar,object_list_camera,overlapThresh):
    xx_lidar = []
    yy_lidar = []
    zz_lidar = []
    scores_lidar = []
    for box_info_lidar in object_list_lidar:
        xx_lidar.append(box_info_lidar['translation'][0])
        yy_lidar.append(box_info_lidar['translation'][1])
        zz_lidar.append(box_info_lidar['translation'][2])
        scores_lidar.append(box_info_lidar['detection_score'])
    scores_lidar = np.array(scores_lidar)
    xx_lidar = np.array(xx_lidar)
    yy_lidar = np.array(yy_lidar)
    zz_lidar = np.array(zz_lidar)
    bbox_lidar = np.array(object_list_lidar)
    
    idxs_scores_lidar = np.argsort(scores_lidar)
    xx_camera = []
    yy_camera = []
    zz_camera = []
    scores_camera = []
    
    for box_info_camera in object_list_camera:
        xx_camera.append(box_info_camera['translation'][0])
        yy_camera.append(box_info_camera['translation'][1])
        zz_camera.append(box_info_camera['translation'][2])
        scores_camera.append(box_info_camera['detection_score'])
    scores_camera = np.array(scores_camera)
    xx_camera = np.array(xx_camera)
    yy_camera = np.array(yy_camera)
    zz_camera = np.array(zz_camera)
    bbox_camera = np.array(object_list_camera)
    
    
    idxs_camera = np.argsort(scores_camera)
    idxs_lidar = np.argsort(scores_lidar)
    pick = []
    #print(idxs)
    #print('len',idxs_camera)
    while len(idxs_camera) > 0 and len(idxs_lidar) > 0:

        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs_camera) - 1
        i = idxs_camera[last]
        
        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.array(xx_lidar[idxs_lidar[:]])
        yy1 = np.array(yy_lidar[idxs_lidar[:]])
        zz1 = np.array(zz_lidar[idxs_lidar[:]])
        xx2 = np.array(xx_camera[i])
        yy2 = np.array(yy_camera[i])
        zz2 = np.array(zz_camera[i])

        # compute the distance
        dis = np.sqrt((xx1-xx2)**2+(yy1-yy2)**2+(zz1-zz2)**2)
        # delete all indexes from the index list that have
        if np.any(np.where(dis<= overlapThresh)):
            pick.append(i)
        #print('delete',np.where(dis <= overlapThresh))
        idxs_lidar = np.delete(idxs_lidar, np.where(dis <= overlapThresh))
        idxs_camera = np.delete(idxs_camera, [last])
        # return only the bounding boxes that were picked
    print('len',len(bbox_camera[pick]))
    cd_result = []
    for indice in pick:
        cd_result.append(object_list_camera[indice])
    return cd_result


# In[29]:
'''

# Intersection cd
detector_1 = 'lidar_mini_val'
detector_2 = 'camera_mini_val'
detector_3 = 'centerpoint_voxel_circlenms_mini_val'
detector_4 = 'pointpillars_secfpn_mini_val'
result_camera,result_lidar,frames = load_mini_val(detector_1,detector_3)
result = {}
result_path = []
result_index = []
for i in range(24):
    for frame in frames[:]:
        object_list_lidar = result_lidar[frame]
        object_list_camera = result_camera[frame]
        result_cd_intersection = cd_intersection(object_list_lidar,object_list_camera,0.2+0.2*i)
        result[frame] = result_cd_intersection
        
    index = 'D1_D3_' + str(round(0.2+0.2*i,2))
    name = 'CD/threshold/'+index
    path = '/Users/mike/Desktop/data/sets/out_put_dir/'+name +'.json'
    
    result_path.append(path)
    result_index.append(index)
    create_and_save_json(result,name)
print(result_path)
print(result_index)


# In[ ]:


# Intersection NMS
detector_1 = 'lidar_mini_val'
detector_2 = 'camera_mini_val'
detector_3 = 'centerpoint_voxel_circlenms_mini_val'
detector_4 = 'pointpillars_secfpn_mini_val'
result_camera,result_lidar,frames = load_mini_val(detector_3,detector_4)
result = {}
for frame in frames[:]:
    object_list_lidar = result_lidar[frame]
    object_list_camera = result_camera[frame]
    result_nms_intersection = NMS_intersection(object_list_lidar,object_list_camera,0.1,0.099)
    result[frame] = result_nms_intersection
name = 'NMS/Inter/D3_D4'
create_and_save_json(result,name)


# In[ ]:





# In[ ]:


#WBF
#cd
#NMS
detector_1 = 'lidar_mini_val'
detector_2 = 'camera_mini_val'
detector_3 = 'centerpoint_voxel_circlenms_mini_val'
detector_4 = 'pointpillars_secfpn_mini_val'
result_camera,result_lidar,frames = load_mini_val(detector_1,detector_2)

result={}
time_start = time.time() #start clock

c = 1
for frame in frames[:]:

    #Prepare box info
    print('frame: ',c)
    c = c+1
    object_list = []
    object_list_lidar = result_lidar[frame]
    object_list_camera = result_camera[frame]
    print(detector_2,' len:',len(object_list_lidar),detector_1,' len:',len(object_list_camera),'len original: ',len(object_list_lidar)+len(object_list_camera))
    for box in object_list_lidar:
        object_list.append(box)
    for box in object_list_camera:
        object_list.append(box)
    
    
    #WBF
    print('WBF Fusion')
    boxes_info = []
    boxes_info.append(object_list_camera)
    boxes_info.append(object_list_lidar)
    wbf_result = weighted_boxes_fusion_3d(boxes_info,weights = [1,1],iou_thr=0.1, skip_box_thr=0.099,intersection=True)
    print('wbf len',len(wbf_result))
    result[frame]=wbf_result
    
    
    
    #NMS
    print("NMS")
    nms_result = non_max_suppression_fast_3d(object_list_lidar,0.425,0)
    result[frame] = nms_result
    
    
    
    #center distance
    print('Center distance')
    cd_result = center_distence(object_list,2)
    result[frame] = cd_result
    
    


time_end = time.time()    #end clock

time_c= time_end - time_start   #time cost
print('time cost', time_c, 's')
name = 'NMS/Union/D1_D2'
create_and_save_json(result,name)


# In[ ]:


print(result)


# In[ ]:


#find best iou for nms
detector_1 = 'lidar_mini_val'
detector_2 = 'camera_mini_val'
result_camera,result_lidar,frames = load_mini_val(detector_1,detector_2)

c = 1
result={}
time_start = time.time() #start clock

for i in range(10):
    c = 1
    for frame in frames[:]:
        

        print('frame: ',c)
        c = c+1
        object_list = []
        object_list_lidar = result_lidar[frame]
        object_list_camera = result_camera[frame]
        #print('len Lidar:',len(object_list_lidar),'len Cam: ',len(object_list_camera),'len original: ',len(object_list_lidar)+len(object_list_camera))
        for box in object_list_lidar:
            object_list.append(box)

        for box in object_list_camera:
            object_list.append(box)
        thr_iou = 0.2+i*0.05
        print('thr_iou',thr_iou)
        #NMS
        nms_result = non_max_suppression_fast_3d(object_list_lidar,thr_iou,0)
        result[frame] = nms_result
        
        #center distence
        
        indices_cd,boxes_cd,score_cd = center_distence(object_list,2)
        print('len',len(boxes_cd))
        temp = []
        for indice in indices_cd:
            temp.append(object_list[indice])
        result[frame] = temp
        


    time_end = time.time()    #end clock

    time_c= time_end - time_start   #time cost
    print('time cost', time_c, 's')
    name = 'lidar_mini_val_nms'+str(round((0.2+i*0.05),3))
    create_and_save_json(result,name)


    


# In[ ]:


detector_1 = 'lidar_mini_val'
detector_2 = 'camera_mini_val'
detector_3 = 'centerpoint_voxel_circlenms_mini_val'
detector_4 = 'pointpillars_secfpn_mini_val'
result = addition(detector_2,detector_4,0.099)
name = 'Add/D2_D4'
create_and_save_json(result,name)


'''
