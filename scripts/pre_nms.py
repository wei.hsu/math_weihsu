#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import run_evaluate
import class_specific_fusion
from evaluate import NuScenesEval
import numpy as np
import json
import evaluate
import build_combination
import general_fusion


# In[2]:


def load_data(path):
    with open(path) as f:
        data = json.load(f)
    result = data['results']
    return result
        


# In[8]:


def pre_nms(sensor,iou):
    sensor = sensor
    path = os.getcwd()+'/dataset/baseline/'
    dirlist = os.listdir(path+sensor)
    file_names = [s.replace('.json','') for s in dirlist]
    print(file_names)

    for name in file_names:
        result = load_data(path+sensor+'/'+name+'.json')
        print(len(result.keys()))
        frames = []
        for i in result.keys():
            frames.append(i)
        #for frame in frames:
        new_result = {}
        for frame in frames:
            boxes_info = []
            object_list = result[frame]
            boxes_info.append(object_list)
            nms_result = class_specific_fusion.non_max_suppression_fast_3d_class(boxes_info,iou,0)
            new_result[frame]=nms_result
        class_specific_fusion.create_and_save_json(new_result,'dataset/prenms_baseline/'+name)


# In[9]:


#pre_nms('lidar',0.14)
#pre_nms('camera',0.14)


# In[3]:


add_result = {}
add_result= general_fusion.addition('dataset/prenms_baseline/pointpillar','dataset/prenms_baseline/mapillary',0.1)
class_specific_fusion.create_and_save_json(add_result,'dataset/prenms_baseline/prenms_add/mapillary&pointpillar_secfpn')

add_result= general_fusion.addition('dataset/prenms_baseline/centerpoint','dataset/prenms_baseline/fcos',0.1)
class_specific_fusion.create_and_save_json(add_result,'dataset/prenms_baseline/prenms_add/fcos&centerpoint')

add_result= general_fusion.addition('dataset/prenms_baseline/pointpillar','dataset/prenms_baseline/fcos',0.1)
class_specific_fusion.create_and_save_json(add_result,'dataset/prenms_baseline/prenms_add/fcos&pointpillar')
# In[5]:



# In[6]:


result_path_s = ['/Users/mike/Desktop/MasterThesis/dataset/prenms_baseline/prenms_add/mapillary&pointpillar_secfpn.json',
                 '/Users/mike/Desktop/MasterThesis/dataset/prenms_baseline/prenms_add/fcos&centerpoint.json',
                 '/Users/mike/Desktop/MasterThesis/dataset/prenms_baseline/prenms_add/fcos&pointpillar.json',
                 ]
index = ['mapillary&pointpillar_secfpn','fcos&centerpoint','fcos&pointpillar']
evaluate.NuScenesEval.start(result_path_s,index,'csv_file/pre_nms_test')


# In[ ]:




