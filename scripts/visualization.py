#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import os
import json
import torch
import math
import numpy as np
from collections import OrderedDict
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import open3d as o3d
#from Ground_truth import load_groundtruth
from nuscenes.eval.common.loaders import load_gt
from nuscenes import NuScenes
from nuscenes.eval.common.data_classes import EvalBoxes
from nuscenes.eval.detection.data_classes import DetectionBox
from nuscenes.eval.detection.utils import category_to_detection_name
from nuscenes.eval.tracking.data_classes import TrackingBox
from nuscenes.utils.data_classes import Box
from nuscenes.utils.geometry_utils import points_in_box
from nuscenes.utils.splits import create_splits_scenes
def load_split(nusc, eval_split):
        """
        :param eval_split: name of the split
        :return: scene and sample tokens belonging to selected split
        """

        # load scene tokens of given split
        splits = create_splits_scenes()
        scene_tokens = [nusc.field2token('scene', 'name', scene_name)[0] for scene_name in splits[eval_split]]

        # load sample tokens of given split
        sample_tokens = []
        for scene_token in scene_tokens:
            sample_tokens += nusc.field2token('sample', 'scene_token', scene_token)

        return scene_tokens, sample_tokens

def quatRotation(W,X,Y,Z):
    """
    Covert a quaternion into a full three-dimensional rotation matrix.
 
    Input
    :param Q: A 4 element array representing the quaternion (q0,q1,q2,q3) 
 
    Output
    :return: A 3x3 element matrix representing the full 3D rotation matrix. 
             This rotation matrix converts a point in the local reference 
             frame to a point in the global reference frame.
    """
    # Extract the values from Q
    q0 = math.cos(W/2)
    q1 = X*math.sin(W/2)
    q2 = Y*math.sin(W/2)
    q3 = Z*math.sin(W/2)
     
    # First row of the rotation matrix
    r00 = 2 * (q0 * q0 + q1 * q1) - 1
    r01 = 2 * (q1 * q2 - q0 * q3)
    r02 = 2 * (q1 * q3 + q0 * q2)
     
    # Second row of the rotation matrix
    r10 = 2 * (q1 * q2 + q0 * q3)
    r11 = 2 * (q0 * q0 + q2 * q2) - 1
    r12 = 2 * (q2 * q3 - q0 * q1)
     
    # Third row of the rotation matrix
    r20 = 2 * (q1 * q3 - q0 * q2)
    r21 = 2 * (q2 * q3 + q0 * q1)
    r22 = 2 * (q0 * q0 + q3 * q3) - 1
     
    # 3x3 rotation matrix
    mat = np.array([[r00, r01, r02],
                           [r10, r11, r12],
                           [r20, r21, r22]])
                            
    return mat


def box_center_to_corner(translation, size, rotation):
    # To return
    corner_boxes = np.zeros((8, 3))

    translation = translation
    h, w, l = size[0], size[1], size[2]
    W,X,Y,Z = rotation

    # Create a bounding box outline
    bounding_box = np.array([
        [-l/2, -l/2, l/2, l/2, -l/2, -l/2, l/2, l/2],
        [w/2, -w/2, -w/2, w/2, w/2, -w/2, -w/2, w/2],
        [-h/2, -h/2, -h/2, -h/2, h/2, h/2, h/2, h/2]])

    # Standard 3x3 rotation matrix around the Z axis
    rotation_matrix = quatRotation(W,X,Y,Z)
    #rotation_matrix = rotation_matrix[:3]
    print(rotation_matrix)
    # Repeat the [x, y, z] eight times
    eight_points = np.tile(translation, (8, 1))

    # Translate the rotated bounding box by the
    # original center position to obtain the final box
    corner_box = np.dot(
        rotation_matrix, bounding_box) + eight_points.transpose()

    return corner_box.transpose()


def visualise_frame(data,frame,color, name):
    i = 0
    result = data['results']
    frame_sensor = []
    object_list = result[frame]
    for box_info in object_list:
        i = i+1
        translation = box_info['translation']
        size = box_info['size']
        rotation = box_info['rotation']
        detection_score = float(box_info["detection_score"])
        detection_name = str(box_info['detection_name'])
        
        corner_box = box_center_to_corner(translation,size,rotation)

        # Our lines span from points 0 to 1, 1 to 2, 2 to 3, etc...
        lines = [[0, 1], [1, 2], [2, 3], [0, 3],
                 [4, 5], [5, 6], [6, 7], [4, 7],
                 [0, 4], [1, 5], [2, 6], [3, 7]]

        # Use the same color for all lines
        colors = [color for _ in range(len(lines))]

        line_set = o3d.geometry.LineSet()
        line_set.points = o3d.utility.Vector3dVector(corner_box)
        line_set.lines = o3d.utility.Vector2iVector(lines)
        line_set.colors = o3d.utility.Vector3dVector(colors)

        # Display the bounding boxes:
        vis.add_geometry(name + str(i) , line_set)
        score = round(detection_score,3)
        x1 = round(corner_box[0][0],3),round(corner_box[0][1],3),round(corner_box[0][2],3)
        x2 = round(corner_box[1][0],3),round(corner_box[1][1],3),round(corner_box[1][2],3)
        x3 = round(corner_box[2][0],3),round(corner_box[2][1],3),round(corner_box[2][2],3)
        x4 = round(corner_box[3][0],3),round(corner_box[3][1],3),round(corner_box[3][2],3)
        x5 = round(corner_box[4][0],3),round(corner_box[4][1],3),round(corner_box[4][2],3)
        x6 = round(corner_box[5][0],3),round(corner_box[5][1],3),round(corner_box[5][2],3)
        x7 = round(corner_box[6][0],3),round(corner_box[6][1],3),round(corner_box[6][2],3)
        x8 = round(corner_box[7][0],3),round(corner_box[7][1],3),round(corner_box[7][2],3)

        
        vis.add_3d_label(corner_box[0], "{}".format(score))
        vis.add_3d_label(corner_box[1], "{}".format(detection_name))
            
#visualization of ground truth        
def visualise_frame_gt(frame,color,name):
        i = 0
        Ground_truth = load_gt(nusc, eval_split,DetectionBox)
        gt_objects = Ground_truth.all
        for gt_object in gt_objects:
            if gt_object.sample_token == frame:
                i = i+1
                corner_box = box_center_to_corner(gt_object.translation,gt_object.size,gt_object.rotation)
                        # Our lines span from points 0 to 1, 1 to 2, 2 to 3, etc...
                lines = [[0, 1], [1, 2], [2, 3], [0, 3],
                         [4, 5], [5, 6], [6, 7], [4, 7],
                         [0, 4], [1, 5], [2, 6], [3, 7]]

                # Use the same color for all lines
                colors = [[0, color, 0] for _ in range(len(lines))]
                
                detection_name = str(gt_object.detection_name)
                
                line_set = o3d.geometry.LineSet()
                line_set.points = o3d.utility.Vector3dVector(corner_box)
                line_set.lines = o3d.utility.Vector2iVector(lines)
                line_set.colors = o3d.utility.Vector3dVector(colors)
                vis.add_geometry(name + str(i) , line_set)
                
                vis.add_3d_label(corner_box[1], "{}".format(detection_name))






if __name__ == "__main__":
    nusc = NuScenes(version='v1.0-mini', dataroot='/Users/mike/Desktop/data/sets/v1.0-mini', verbose=True)
    eval_split = 'mini_val'

    
    with open(os.getcwd() + '/dataset/baseline/mapillary.json') as f:
        data_camera = json.load(f)
    with open(os.getcwd() +'/dataset/baseline/pointpillars.json') as d:
        data_lidar = json.load(d)
        
    with open(os.getcwd() +'/dataset/baseline/GT.json') as e:
        data_myfile = json.load(e)
    '''
    with open('/Users/mike/Desktop/data/sets/out_put_dir/pointpillars-val.json') as f:
        data_camera = json.load(f)
    with open('/Users/mike/Desktop/data/sets/out_put_dir/result/cd_thr2.json') as d:
        data_lidar = json.load(d)
        '''
    #with open('/Users/mike/Desktop/data/sets/out_put_dir/myfile/mini_fused/mini.json') as e:
     #   data_myfile = json.load(e)

    
    result_camera = data_camera['results']
    result_lidar = data_lidar['results']
    frame_camera = []
    frame_lidar = []
    for i in result_camera.keys():
        frame_camera.append(i)
    for j in result_lidar.keys():
        frame_lidar.append(j)
    scene_tokens, sample_tokens = load_split(nusc, eval_split)
    print(sample_tokens)

    #frame = '3e8750f331d7499e9b5123e9eb70f2e2'
    
    #frame selection
    frame = sample_tokens[10]
    print('frma:',  frame)

    app = o3d.visualization.gui.Application.instance
    app.initialize()

    # Create a visualization object and window
    vis = o3d.visualization.O3DVisualizer()
    vis.show_settings = True
    #vis.create_window()    
    visualise_frame(data_camera,frame,[1,0,0], "red")
    visualise_frame(data_lidar,frame,[0,0,1], 'blue')
    #visualise_frame(data_myfile,frame,[1,0,0], 'r')
    visualise_frame_gt(frame,'1','g')
    #vis.run()
    #vis.destroy_window()

    vis.reset_camera_to_default()
    #vis_reset_camera_to_default()

    app.add_window(vis)
    app.run()

