#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import class_specific_fusion
from evaluate import NuScenesEval
import numpy as np
import json
import evaluate
import general_fusion


# In[2]:


def load_data(path):
    with open(path) as f:
        data = json.load(f)
    result = data['results']
    return result


#/Users/mike/Desktop/MasterThesis/dataset/IoU
#IoU Threshold testing
def threshold_test_IoU(result,name):
    evaluate_files = []
    evaluate_index = []
    for i in range(24,25,1):
        c = 0
        frames = []
        fused_result = {}
        for j in result.keys():
            frames.append(j) 
        for frame in frames[:]:
            c = c+1
            print('frame:',c)
            boxes_info = []
            object_list = result[frame]
            boxes_info.append(object_list)
            #NMS
            nms_result = class_specific_fusion.non_max_suppression_fast_3d_class(boxes_info,0.02+i*0.02,0.1)
            fused_result[frame] = nms_result
        test = round(0.02+i*0.02,3)
        same_sensor = 'same_sensor'
        file_name = 'dataset/IoU/same_sensor/IoU_threshold/'+name+str(test)
        print('save name',name+str(test))
        class_specific_fusion.create_and_save_json(fused_result,file_name)
        
        #create path for evaluation 
        #for evaluation process
        evaluate_files.append(os.getcwd()+'/'+file_name+'.json')
        evaluate_index.append(name+str(test))
    return evaluate_files,evaluate_index



#/Users/mike/Desktop/MasterThesis/dataset/center_distance
#IoU Threshold testing
def threshold_test_cd(result,name):
    evaluate_files = []
    evaluate_index = []
    for i in range(25):
        c = 0
        frames = []
        fused_result = {}
        for j in result.keys():
            frames.append(j) 
        for frame in frames[:]:
            c = c+1
            print('frame:',c)
            boxes_info = []
            object_list = result[frame]
            boxes_info.append(object_list)
            
            #center distance:1
            #skip threshold:0.1
            cd_result = class_specific_fusion.center_distence_class(boxes_info,0.2+i*0.2,0.1) 
            fused_result[frame] = cd_result
        test = round(0.2+0.2*i,2)
        same_sensor = 'same_sensor'
        file_name = 'dataset/center_distance/same_sensor/cd_threshold/'+name+str(test)

        print('save name',name+str(test))
        class_specific_fusion.create_and_save_json(fused_result,file_name)
        
        #create path for evaluation 
        #for evaluation process
        evaluate_files.append(os.getcwd()+'/'+file_name+'.json')
        evaluate_index.append(name+str(test))

    return evaluate_files,evaluate_index

def weight_test(result,name):
    baseline = {'fcos':0.307584951,'mapillary':0.264107513,'pointpillars':0.338579854
                         ,'pointpillars_secfpn':0.338394308,'centerpoint':0.48588735}
    
    best_iou = {'mapillary&pointpillars_secfpn':0.14,'fcos&pointpillars_secfpn':0.18,'mapillary&pointpillars':0.16
                ,'mapillary&centerpoint':0.06,'fcos&pointpillars':0.14,'fcos&centerpoint':0.14
                }
    evaluate_files = []
    evaluate_index = []
    w = name.split("&", 1)
    total = baseline[w[0]]+baseline[w[1]]
    weights = [[baseline[w[0]]/total,baseline[w[1]]/total],[1,1]]
    iou = best_iou[name]
    #iou = 0.425
    frames = []
    fused_result = {}
    for j in result.keys():
        frames.append(j) 
    for weight in weights:
        c = 0
        print('weight:',weight)
        for frame in frames[:]:
            c = c+1
            print('frame:',c)
            boxes_info = []
            object_list = result[frame]
            boxes_info.append(object_list)

            wbf_result = class_specific_fusion.weighted_boxes_fusion_3d(object_list, weight, iou
                                                  , 0.1, False)
            #boxes_info, weights, iou_thr, skip_box_thr=0.1,intersection, conf_type='max', allows_overflow=False):
            print('wbf len',len(wbf_result))
            fused_result[frame] = wbf_result
            
        test = '_weighted'+str(weight)
        same_sensor = 'same_sensor'
        file_name = 'dataset/wbf/wbf_proportional/'+name+str(test)

        print('save name',name+str(test))
        class_specific_fusion.create_and_save_json(fused_result,file_name)   
    
    
        evaluate_files.append(os.getcwd()+'/'+file_name+'.json')
        evaluate_index.append(name+str(test))
    return evaluate_files,evaluate_index

def center_distance_intersection(result,name):
    frames = []
    fused_result = {}
    evaluate_files = []
    evaluate_index = []
    for j in result.keys():
        frames.append(j) 
    for i in range(25):
        for frame in frames[:]:
            detector1 = result[frame][0]
            detector2 = result[frame][1]
            test = round(0.2+0.2*i,2)
            cd_result = general_fusion.cd_intersection(detector1,detector2,test)
            fused_result[frame] = cd_result
            
            

        file_name = 'dataset/center_distance/intersection/'+name+str(test)

        print('save name',name+str(test))
        class_specific_fusion.create_and_save_json(fused_result,file_name)
        
        #create path for evaluation 
        #for evaluation process
        evaluate_files.append(os.getcwd()+'/'+file_name+'.json')
        evaluate_index.append(name+str(test))

    return evaluate_files,evaluate_index

def iou_intersection(result,name):
    frames = []
    fused_result = {}
    evaluate_files = []
    evaluate_index = []
    for j in result.keys():
        frames.append(j) 
    for i in range(25):
        for frame in frames[:]:
            detector1 = result[frame][0]
            detector2 = result[frame][1]
            test = round(0.02+0.02*i,3)
            iou_result = general_fusion.NMS_intersection(detector1,detector2,test)
            fused_result[frame] = iou_result
            
            

        file_name = 'dataset/IoU/intersection/'+name+str(test)

        print('save name',name+str(test))
        class_specific_fusion.create_and_save_json(fused_result,file_name)
        
        #create path for evaluation 
        #for evaluation process
        evaluate_files.append(os.getcwd()+'/'+file_name+'.json')
        evaluate_index.append(name+str(test))

    return evaluate_files,evaluate_index


# In[ ]:
def main():
    #folder_name = 'camera&lidar'
    #folder_name = 'lidar'
    #folder_name = 'camera'
    folder_name = 'combination_wbf/lidar'
    folder_name = 'combination_wbf/camera'
    path = os.getcwd()+'/dataset/combination/'+folder_name
    dirlist = os.listdir(path)
    names = [s.replace('.json','') for s in dirlist]
    print(names)
    evaluate_files_list = []
    evaluate_index_list = []
    for name in names:
        result = load_data(path+'/'+name+'.json')
        #print(len(result.keys()))
        '''
        #Union
        #iou
        evaluate_files,evaluate_index = threshold_test_IoU(result,name)
        
        #cd
        evaluate_files,evaluate_index = threshold_test_cd(result,name)
        
        #weight test
        evaluate_files,evaluate_index = weight_test(result,name)
        '''
        
        #intersection
        evaluate_files,evaluate_index = center_distance_intersection(result,name)
        #evaluate_files,evaluate_index = iou_intersection(result,name)
        
        
        evaluate_files_list = evaluate_files_list + evaluate_files
        evaluate_index_list = evaluate_index_list + evaluate_index


    print(evaluate_files_list)
    print(evaluate_index_list)
    print(len(evaluate_files_list))
    print(len(evaluate_index_list))

    result_path_s = evaluate_files_list
    index = evaluate_index_list
    evaluate.NuScenesEval.start(result_path_s,index,'threshold_cd_intersection_camera')

main()




