#!/usr/bin/env python
# coding: utf-8

# In[1]:


import class_specific_fusion
import os
import json


# In[2]:


def load_detector(sensor):
    
    path = os.getcwd()+'/dataset'
    dirlist = os.listdir(path+sensor)
    baseline_paths = []
    detectors = []
    frames = []
    results = {}
    for i in dirlist:
        baseline_path =  path+sensor+'/'+i
        baseline_paths.append(baseline_path)
        detector = i.replace('.json','')
        print(baseline_path)
        with open(baseline_path) as f:
            data = json.load(f)
        result = data['results']
        results[detector] = result

        detectors.append(detector)
    for i in result.keys():
        frames.append(i)
    print(results.keys())
    print(detectors)

    return detectors,results,frames


# In[3]:


def combi_diff_sensor(detectors_camera,detectors_lidar):
    combinations = []
    for i in range(len(detectors_camera)):
        for j in range(len(detectors_lidar)):
            combinations.append([detectors_camera[i],detectors_lidar[j]])
    return combinations


# In[4]:


def combi_single_sensor(detectors):
    combinations = []
    for i in range(len(detectors)):
        for j in range(i+1,len(detectors)):
            combinations.append([detectors[i],detectors[j]])
    return combinations


# In[5]:


def combi_result(combination,result,frames,sensor):
    result_1 = result[combination[0]]
    result_2 = result[combination[1]]
    result={}
    for frame in frames[:]:
        object_list = []
        object_list_1 = result_1[frame]
        object_list_2 = result_2[frame]
        for box in object_list_1:
            object_list.append(box)
        for box in object_list_2:
            object_list.append(box)   
        result[frame] = object_list
    name = combination[0]+'&'+combination[1]
    path = os.getcwd()+'/dataset/combination/' + sensor +'/'
    if not os.path.isdir(path):
        os.mkdir(path)
        
    class_specific_fusion.create_and_save_json(result,'dataset/combination/' + sensor +'/'+name)
    return 0
#weight box fusion
#two detectors muss seperate
#save to ./combination/combination_wbf/
def wbf_combi(combination,result,frames,sensor):
    result_1 = result[combination[0]]
    result_2 = result[combination[1]]
    result={}
    for frame in frames:

        object_list_1 = result_1[frame]
        object_list_2 = result_2[frame]
        wbf_result = []
        wbf_result.append(object_list_1)
        wbf_result.append(object_list_2)
        result[frame] = wbf_result
    name = combination[0]+'&'+combination[1]
    class_specific_fusion.create_and_save_json(result,'dataset/combination/combination_wbf/'+sensor+'/'+name)
    print('wbf save:','/dataset/combination/combination_wbf/'+sensor+'/'+name)
    return 0

def merge(x, y):
    z = x.copy()   # start with keys and values of x
    z.update(y)    # modifies z with keys and values of y
    return z


# In[7]:


def main():
    print('combination start')
    detectors_camera,results_camera,frames_camera = load_detector('/baseline/camera')
    detectors_lidar,results_lidar,frames_lidar = load_detector('/baseline/lidar')
    frames = frames_camera
    combi_lidar = combi_single_sensor(detectors_lidar)
    combi_camera = combi_single_sensor(detectors_camera)

    for combi in combi_lidar:
        combi_result(combi,results_lidar,frames,'lidar')
        wbf_combi(combi,results_lidar,frames,'lidar')
    for combi in combi_camera:
        combi_result(combi,results_camera,frames,'camera')
        wbf_combi(combi,results_camera,frames,'camera')

    combis = combi_diff_sensor(detectors_camera,detectors_lidar)
    results = merge(results_lidar, results_camera)
    wbf_results = []
    wbf_results.append
    for combi in combis:
        combi_result(combi,results,frames,'camera&lidar')
        wbf_combi(combi,results,frames,'camera&lidar')


# In[8]:


main()


# In[ ]:




