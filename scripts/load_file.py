#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import run_evaluate
import class_specific_fusion
from evaluate import NuScenesEval
import numpy as np
import json
import evaluate
from nuscenes import NuScenes
from nuscenes.eval.common.config import config_factory
from nuscenes.eval.common.data_classes import EvalBoxes
from nuscenes.eval.common.loaders import load_prediction, load_gt, add_center_dist, filter_eval_boxes
from nuscenes.eval.detection.algo import accumulate, calc_ap, calc_tp
from nuscenes.eval.detection.constants import TP_METRICS
from nuscenes.eval.detection.data_classes import DetectionConfig, DetectionMetrics, DetectionBox,     DetectionMetricDataList
from nuscenes.eval.detection.render import summary_plot, class_pr_curve, class_tp_curve, dist_pr_curve, visualize_sample


# In[2]:


def load_sensor(sensor):
    sensor = sensor #lidar/camera
    
    path = os.getcwd()+'/dataset/combination/'
    dirlist = os.listdir(path+sensor)
    baseline_paths = []
    detectors = []
    frames = []
    results = {}
    for i in dirlist:
        baseline_path =  path+sensor+'/'+i
        baseline_paths.append(baseline_path)
        detector = i.replace('.json','')
        with open(baseline_path) as f:
            data = json.load(f)
        result = data['results']
        results[detector] = result

        detectors.append(detector)
    for i in result.keys():
        frames.append(i)
    print(results.keys())


    return detectors,results,frames


# In[ ]:





# In[3]:


def same_sensor(detectors,results,frames):
    combinations = []
    evaluate_file_path = '/Users/mike/Desktop/MasterThesis/Result/'
    evaluate_files = []
    evaluate_index = []
    fused_result = {}

    c = 0
    for i in range(len(detectors)):
        for j in range(i+1,len(detectors)):
            combinations.append([detectors[i],detectors[j]])


    for combination in combinations:
        print('combination',combination)
        for frame in frames[:2]:
            c = c+1
            print('frame:',c)
            boxes_info = []
            for name in combination:
                object_list = results[name][frame]
                boxes_info.append(object_list)
            #center distance:1
            #skip threshold:0.1
            #cd_result = class_specific_fusion.center_distence_class(boxes_info,1,0.1) 
            nms_result = class_specific_fusion.non_max_suppression_fast_3d_class(boxes_info,0.425,0.1) 
            fused_result[frame] = nms_result
        name = combination[0]+'&'+combination[1]
        print('save name',name)
        class_specific_fusion.create_and_save_json(fused_result,name)
        evaluate_files.append(evaluate_file_path+name+'.json')
        evaluate_index.append(name)
    return (evaluate_files,evaluate_index)


# In[4]:



detectors_camera,results_camera,frames = load_sensor('camera')
detectors_lidar,results_lidar,frames = load_sensor('lidar')
print(len(frames))
'''
evaluate_files,evaluate_index = same_sensor(detectors_camera,results_camera,frames)
evaluate_files,evaluate_index = same_sensor(detectors_lidar,results_lidar,frames)
'''


# In[ ]:


'''
combinations = []
evaluate_file_path = '/Users/mike/Desktop/MasterThesis/Result/'
evaluate_files = []
evaluate_index = []
fused_result = {}
results = {}
results_camera.update(results_lidar)
results.update(results_camera)
print(results.keys())

for i in range(len(detectors_camera)):
    for j in range(len(detectors_lidar)):
        combinations.append([detectors_camera[i],detectors_lidar[j]])

for combination in combinations:
    c = 0
    print('combination',combination)
    for frame in frames[:2]:
        c = c+1
        print('frame:',c)
        boxes_info = []
        for name in combination:
            object_list = results[name][frame]
            boxes_info.append(object_list)
        #center distance:1
        #skip threshold:0.099
        #cd_result = class_specific_fusion.center_distence_class(boxes_info,0.6,0.1) 
        #fused_result[frame] = cd_result
        nms_result = class_specific_fusion.non_max_suppression_fast_3d_class(boxes_info,0.425,0.1)
        fused_result[frame] = nms_result
    name = combination[0]+'&'+combination[1]
    print('save name',name)
    class_specific_fusion.create_and_save_json(fused_result,name)
    evaluate_files.append(evaluate_file_path+name+'.json')
    evaluate_index.append(name)

'''

# In[ ]:


#IoU Threshold testing
combinations = []
evaluate_file_path = os.getcwd()+'/'
evaluate_files = []
evaluate_index = []
fused_result = {}
results = {}
results_camera.update(results_lidar)
results.update(results_camera)
print(results.keys())

for i in range(len(detectors_camera)):
    for j in range(len(detectors_lidar)):
        combinations.append([detectors_camera[i],detectors_lidar[j]])

    for i in range(1):
        c = 0
        for frame in frames[:]:
            c = c+1
            print('frame:',c)
            boxes_info = []
            for name in combination:
                object_list = results[name][frame]
                boxes_info.append(object_list)
            #center distance:1
            #skip threshold:0.099
            cd_result = class_specific_fusion.center_distence_class(boxes_info,0.6,0.1) 
            fused_result[frame] = cd_result
            #nms_result = class_specific_fusion.non_max_suppression_fast_3d_class(boxes_info,0.02+i*0.02,0.1)
            #fused_result[frame] = nms_result
            
        #save to path: cwd+name
        #/folder/
        name = combination[0]+'&'+combination[1]+str(round(0.02+i*0.02,2))
        print('save name',name)
        class_specific_fusion.create_and_save_json(fused_result,name)



        evaluate_files.append(evaluate_file_path+name+'.json')
        evaluate_index.append(name)



print(evaluate_files)
print(evaluate_index)

print(len(evaluate_files))
print(len(evaluate_index))


result_path_s = evaluate_files
index = evaluate_index
evaluate.NuScenesEval.start(result_path_s,index,'score')


# In[ ]:





# In[ ]:




